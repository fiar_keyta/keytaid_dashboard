<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Canvaser
{
    public function userList($token, $page, $name, $shop_id, $ref_code, $tag, $keyword, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25,
            'canvaser' => $name,
            'shop_id' => $shop_id,
            'refferal_code' => $ref_code,
            'tag' => $tag,
            'keyword' => $keyword,
            'date_range' => $date_range
        ];
        $response = $client->request('GET', config('constants.api_url').'/users/canvaser' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function userExport($token, $ref_code)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'refferal_code' => $ref_code
        ];
        $response = $client->request('GET', config('constants.api_url').'/users/canvaser/export' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function ordersList($token, $page, $name, $shop_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25,
            'canvaser' => $name,
            'shop_id' => $shop_id
        ];
        $response = $client->request('GET', config('constants.api_url').'/orders/canvaser' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }
}