<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Order
{
    public function ordersList($token, $page, $expedition_name, $payment_method, $voucher_title, $date_range, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        if(isset($expedition_name)){
            $adder = ['expedition_name' => $expedition_name];
            $body = $body + $adder;
        }
        if(isset($date_range)){
            $adder = ['date_range' => $date_range];
            $body = $body + $adder;
        }
        if(isset($payment_method)){
            $adder = ['payment_method' => $payment_method];
            $body = $body + $adder;
        }
        if(isset($voucher_title)){
            $adder = ['voucher_title' => $voucher_title];
            $body = $body + $adder;
        }
        if(isset($keyword)){
            $adder = ['keyword' => $keyword];
            $body = $body + $adder;
        }
        
        $response = $client->request('GET', 'https://live.keyta.id/api/v2/idb/order/couriers' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        
        return $orders['data'];
    }

    public function orderDetails($token, $order_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $response = $client->request('GET', config('constants.api_url')."/orders/detail/{$order_id}" , [
            'headers' => $headers, 
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        
        return $orders;
    }

    public function ordersExport($token, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'date_range' => $date_range, 
        ];
        $response = $client->request('GET', config('constants.api_url').'/orders/export' , [
            'headers' => $headers,
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function ordersSearch($token, $keyword, $page,$expedition_name, $payment_method, $voucher_title)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        if(isset($expedition_name)){
            $adder = ['expedition_name' => $expedition_name];
            $body = $body + $adder;
        }
        if(isset($keyword)){
            $adder = ['keyword' => $keyword];
            $body = $body + $adder;
        }
        if(isset($payment_method)){
            $adder = ['payment_method' => $payment_method];
            $body = $body + $adder;
        }
        if(isset($voucher_title)){
            $adder = ['voucher_title' => $voucher_title];
            $body = $body + $adder;
        }

        $response = $client->request('GET', config('constants.api_url').'/orders/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function ordersCancel($token, $order_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        // $body = [
        //     'order_id' => $order_id;
        // ];
        $response = $client->request('PUT', config('constants.api_url')."/orders/{$order_id}/cancel" , [
            'headers' => $headers, 
            // 'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function ordersListDifference($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];
        
        $response = $client->request('GET', config('constants.api_url').'/order/difference/list' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        return $orders;
    }

    public function createDifference($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('POST', config('constants.api_url')."/order/difference" , [
            'headers' => $headers, 
            'json' => $data,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        return $orders;
    }

}