<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Keytapoint
{
    public function pointsList($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        $response = $client->request('GET', config('constants.api_url').'/keyta_point/user/balance' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $kp = json_decode($response->getBody()->getContents(), true);

        return $kp;
    }

    public function pointsListSearch($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $body = [
            'page' => $page, 
            'per_page' => 25,
            'keyword' => $keyword
        ];

        $response = $client->request('GET', config('constants.api_url').'/keyta_point/user/balance/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $kp = json_decode($response->getBody()->getContents(), true);

        return $kp;
    }

    public function pointHistory($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        $response = $client->request('GET', config('constants.api_url').'/keyta_point/user/balance/history' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $costs = json_decode($response->getBody()->getContents(), true);

        return $costs;
    }

    public function pointHistorySearch($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $body = [
            'keyword' => $keyword,
            'page' => $page, 
            'per_page' => 25
        ];

        $response = $client->request('GET', config('constants.api_url').'/keyta_point/user/balance/history/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $costs = json_decode($response->getBody()->getContents(), true);

        return $costs;
    }

    public function pointHistoryExport($token, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [ 
            'date_range' => $date_range,
        ];
        $response = $client->request('GET', config('constants.api_url').'/keyta_point/user/balance/history/export' , [
            'headers' => $headers,
            'json' => $body,
        ]);
        $points = json_decode($response->getBody()->getContents(), true);

        return $points;
    }

    public function checksum($token, $shop_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "shop_id" => $shop_id
        ];
        $response = $client->request('GET', config('constants.api_url').'/keyta_point/checksum' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $checksum = json_decode($response->getBody()->getContents(), true);

        return $checksum["hash"];
    }

    public function kpTopup($token, $shop_id, $value, $checksum)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "shop_id" => $shop_id,
            "topup_type" => "topup",
            "checksum" => $checksum,
            "topup_value" => (int)$value
        ];

        $response = $client->request('POST', config('constants.api_url').'/keyta_point/balance/topup' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $topup = json_decode($response->getBody()->getContents(), true);

        return $topup;
    }

    public function kpreward($token, $shop_id, $value, $checksum, $description)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "shop_id" => $shop_id,
            "topup_type" => "reward",
            "checksum" => $checksum,
            "topup_value" => (int)$value,
            "reward_description" => $description
        ];

        $response = $client->request('POST', config('constants.api_url').'/keyta_point/balance/reward' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $topup = json_decode($response->getBody()->getContents(), true);

        return $topup;
    }

    public function kpReduce($token, $shop_id, $value, $checksum)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "shop_id" => $shop_id,
            "topup_type" => "reduce",
            "checksum" => $checksum,
            "reduce_value" => (int)$value
        ];

        $response = $client->request('PUT', config('constants.api_url').'/keyta_point/balance/reduce' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $reduce = json_decode($response->getBody()->getContents(), true);

        return $reduce;
    }
}