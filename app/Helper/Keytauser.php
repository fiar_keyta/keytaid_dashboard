<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Keytauser
{
    public function userList($token, $page, $community_name, $sort_by, $sort_table_name, $sort_column_name, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25,
            'table_name' => $sort_table_name ? $sort_table_name : "users",
            'table_column' => $sort_column_name ? $sort_column_name : "id",
            'sort_by' => $sort_by ? $sort_by : "DESC",
        ];
        
        if(isset($date_range)){
            $adder = ['date_range' => $date_range];
            $body = $body + $adder;
        }

        if(isset($community_name)){
            $adder = ['community_name' => $community_name];
            $body = $body + $adder;
        }

        $response = $client->request('GET', config('constants.api_url').'/users' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function communityList($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/community/list/name' , [
            'headers' => $headers, 
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function dashboard($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/dashboard' , [
            'headers' => $headers
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        return $orders;
    }

    public function userExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/users/export' , [
            'headers' => $headers
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function userBalanceExport($token, $shop_id_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [ 
            'shop_id_range' => $shop_id_range,
        ];
        $response = $client->request('GET', config('constants.api_url').'/keyta_point/user/balance/export' , [
            'headers' => $headers,
            'json' => $body
        ]);
        $usersBalance = json_decode($response->getBody()->getContents(), true);

        return $usersBalance;
    }

    public function userSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'keyword' => $keyword, 
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/users/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function referralList($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/referral' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);

        return $users;
    }

    public function userCommunity($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25,
        ];

        $response = $client->request('GET', config('constants.api_url').'/users/community/canvaser?referrer=4301DZ0D0JQ7,LUNASUMKM' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);
        // dd($users);
        return $users;
    }

    public function userCommunityInv($token, $shop_id, $page, $referrer)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25,
            'shop_id' => $shop_id,
        ];

        $response = $client->request('GET', config('constants.api_url')."/users/community/canvaser/inv?referrer={$referrer}" , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $users = json_decode($response->getBody()->getContents(), true);
        // dd($users);
        return $users;
    }
}