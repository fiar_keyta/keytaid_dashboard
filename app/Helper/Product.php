<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Product
{
    public function productsList($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        if(isset($expedition_name)){
            $adder = ['expedition_name' => $expedition_name];
            $body = $body + $adder;
        }
        if(isset($payment_method)){
            $adder = ['payment_method' => $payment_method];
            $body = $body + $adder;
        }
        $response = $client->request('GET', config('constants.api_url').'/products' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $products = json_decode($response->getBody()->getContents(), true);

        return $products;
    }

    public function productsExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/products/export' , [
            'headers' => $headers
        ]);
        $products = json_decode($response->getBody()->getContents(), true);

        return $products;
    }

    public function productsSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'keyword' => $keyword, 
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/products/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $products = json_decode($response->getBody()->getContents(), true);

        return $products;
    }
}