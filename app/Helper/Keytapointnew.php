<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Keytapointnew
{
    public function pointsList($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJrZXl0YXByb2R1Y3Rpb24iLCJpYXQiOjE2NDM5Njk2MDh9.2IlksExSOiaYH1DuMuf_juXHEshukIJe7Yq05fObmdE" ];

        $response = $client->request('GET', 'https://live.keyta.id/wallet/v1/point-service/users-balance?page_number='.$page.'&limit=25&keyword='.$keyword, [
            'headers' => $headers, 
        ]);
        $kp = json_decode($response->getBody()->getContents(), true);

        return $kp['data'];
    }

    public function pointHistory($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJrZXl0YXByb2R1Y3Rpb24iLCJpYXQiOjE2NDM5Njk2MDh9.2IlksExSOiaYH1DuMuf_juXHEshukIJe7Yq05fObmdE" ];

        $response = $client->request('GET', 'https://live.keyta.id/wallet/v1/point-service/users-history?page_number='.$page.'&limit=25&keyword='.$keyword , [
            'headers' => $headers, 
        ]);
        $histories = json_decode($response->getBody()->getContents(), true);

        return $histories['data'];
    }

    public function pointHistorySearch($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $body = [
            'keyword' => $keyword,
            'page' => $page, 
            'per_page' => 25
        ];

        $response = $client->request('GET', config('constants.api_url').'/keyta_point/user/balance/history/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $costs = json_decode($response->getBody()->getContents(), true);

        return $costs;
    }

    public function pointHistoryExport($token, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [ 
            'date_range' => $date_range,
        ];
        $response = $client->request('GET', config('constants.api_url').'/keyta_point/user/balance/history/export' , [
            'headers' => $headers,
            'json' => $body,
        ]);
        $points = json_decode($response->getBody()->getContents(), true);

        return $points;
    }

    public function checksum($token, $shop_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "shop_id" => $shop_id
        ];
        $response = $client->request('GET', config('constants.api_url').'/keyta_point/checksum' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $checksum = json_decode($response->getBody()->getContents(), true);

        return $checksum["hash"];
    }

    public function kpTopup($token, $shop_id, $value, $checksum)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "shop_id" => $shop_id,
            "topup_type" => "topup",
            "checksum" => $checksum,
            "topup_value" => (int)$value
        ];

        $response = $client->request('POST', config('constants.api_url').'/keyta_point/balance/topup' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $topup = json_decode($response->getBody()->getContents(), true);

        return $topup;
    }

    public function kpreward($token, $shop_id, $value, $checksum, $description)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "shop_id" => $shop_id,
            "topup_type" => "reward",
            "checksum" => $checksum,
            "topup_value" => (int)$value,
            "reward_description" => $description
        ];

        $response = $client->request('POST', config('constants.api_url').'/keyta_point/balance/reward' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $topup = json_decode($response->getBody()->getContents(), true);

        return $topup;
    }

    public function kpReduce($token, $shop_id, $value, $checksum)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $body = [
            "shop_id" => $shop_id,
            "topup_type" => "reduce",
            "checksum" => $checksum,
            "reduce_value" => (int)$value
        ];

        $response = $client->request('PUT', config('constants.api_url').'/keyta_point/balance/reduce' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $reduce = json_decode($response->getBody()->getContents(), true);

        return $reduce;
    }
}