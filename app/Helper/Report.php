<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Report
{
    public function reportsList($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        if(isset($expedition_name)){
            $adder = ['expedition_name' => $expedition_name];
            $body = $body + $adder;
        }
        if(isset($payment_method)){
            $adder = ['payment_method' => $payment_method];
            $body = $body + $adder;
        }
        $response = $client->request('GET', config('constants.api_url').'/report' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $reports = json_decode($response->getBody()->getContents(), true);

        return $reports;
    }

    public function reportsExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/report/export' , [
            'headers' => $headers
        ]);
        $reports = json_decode($response->getBody()->getContents(), true);

        return $reports;
    }

    public function reportsSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'keyword' => $keyword, 
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/report/search?keyword='.$keyword , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $reports = json_decode($response->getBody()->getContents(), true);

        return $reports;
    }

    public function reportsResolve($token, $id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'id' => $id
        ];
        $response = $client->request('POST', config('constants.api_url')."/report/{$id}/resolve" , [
            'headers' => $headers
        ]);
        $reports = json_decode($response->getBody()->getContents(), true);

        return $reports;
    }
}