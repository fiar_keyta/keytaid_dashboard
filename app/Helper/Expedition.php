<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Expedition
{
    public function expeditionsList($token, $page, $filter)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = ['page' => $page, 'per_page' => 25, 'filter' => $filter];
        $response = $client->request('GET', config('constants.api_url').'/expeditions/list' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function expeditionsExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/expeditions/export' , [
            'headers' => $headers
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function expeditionsSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'keyword' => $keyword, 
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/expeditions/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function list($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = ['page' => 1, 'per_page' => 25];
        $response = $client->request('GET', config('constants.api_url').'/expeditions' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function list_by_id($token, $id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = ['page' => 1, 'per_page' => 25];
        $response = $client->request('GET', config('constants.api_url').'/expeditions/'.$id , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $expeditions = json_decode($response->getBody()->getContents(), true);

        return $expeditions;
    }

    public function listUpdate($token, $id, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('PUT', config('constants.api_url').'/expeditions/'.$id , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $expedition = json_decode($response->getBody()->getContents(), true);

        return $expedition;
    }
}