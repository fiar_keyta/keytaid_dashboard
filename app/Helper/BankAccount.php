<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class BankAccount
{
    public function bankAccountList($token, $page, $filter)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = ['page' => $page, 'per_page' => 25, 'filter' => $filter];
        $response = $client->request('GET', config('constants.api_url').'/bank_accounts' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $banks = json_decode($response->getBody()->getContents(), true);

        return $banks;
    }

    public function bankAccountExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/bank_accounts/export' , [
            'headers' => $headers
        ]);
        $banks = json_decode($response->getBody()->getContents(), true);

        return $banks;
    }

    public function bankAccountSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'keyword' => $keyword, 
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/bank_accounts/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $banks = json_decode($response->getBody()->getContents(), true);

        return $banks;
    }
}