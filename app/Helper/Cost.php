<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Cost
{
    public function costsList($token, $page, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        if(isset($date_range)){
            $adder = ['date_range' => $date_range];
            $body = $body + $adder;
        }

        $response = $client->request('GET', config('constants.api_url').'/cost' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $costs = json_decode($response->getBody()->getContents(), true);

        return $costs;
    }

    public function costsExport($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/cost/export' , [
            'headers' => $headers
        ]);
        $costs = json_decode($response->getBody()->getContents(), true);

        return $costs;
    }

    public function costsSearch($token, $keyword, $page, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'keyword' => $keyword, 
            'date_range' => $date_range,
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/cost/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $costs = json_decode($response->getBody()->getContents(), true);

        return $costs;
    }
}