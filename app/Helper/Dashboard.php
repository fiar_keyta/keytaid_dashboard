<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Dashboard
{
    public function dashboard($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/dashboard' , [
            'headers' => $headers
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);
        return $orders;
    }

    public function nsm($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url').'/nsm/data' , [
            'headers' => $headers
        ]);
        $nsm = json_decode($response->getBody()->getContents(), true);

        return $nsm;
    }

    public function gross_income($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = ['keyword' => 'COMPLETED,PAID,SUCCESS_COMPLETED,settlement'];
        $response = $client->request('GET', config('constants.api_url').'/nsm/grossincome' , [
            'headers' => $headers,
            'json' => $body
        ]);
        $income = json_decode($response->getBody()->getContents(), true);

        return $income;
    }


}