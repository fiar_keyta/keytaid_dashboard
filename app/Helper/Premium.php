<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Premium
{
    public function premiumOrdersList($token, $page, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25,
            'date_range' => $date_range,
        ];
        $response = $client->request('GET', 'https://live.keyta.id/api/v2/idb/order/premiums', [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders['data'];
    }

    public function ordersExport($token, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [ 
            'date_range' => $date_range,
        ];
        $response = $client->request('GET', config('constants.api_url').'/premiums/export' , [
            'headers' => $headers,
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function premiumOrdersSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'keyword' => $keyword, 
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/premium/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function ordersCancel($token, $order_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        // $body = [
        //     'order_id' => $order_id;
        // ];
        $response = $client->request('PUT', config('constants.api_url')."/orders/{$order_id}/cancel" , [
            'headers' => $headers, 
            // 'json' => $body,
        ]);
        $orders = json_decode($response->getBody()->getContents(), true);

        return $orders;
    }

    public function premiumPriceList($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        // $body = [
        //     'page' => $page, 
        //     'per_page' => 25
        // ];

        $response = $client->request('GET', config('constants.api_url').'/premium/price/list' , [
            'headers' => $headers, 
            // 'json' => $body,
        ]);
        $pf_price = json_decode($response->getBody()->getContents(), true);

        return $pf_price;
    }

    public function premiumPriceListId($token, $pricing_id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        // $body = [
        //     'page' => $page, 
        //     'per_page' => 25
        // ];

        $response = $client->request('GET', config('constants.api_url').'/premium/price/list/'.$pricing_id , [
            'headers' => $headers, 
            // 'json' => $body,
        ]);
        $pf_price = json_decode($response->getBody()->getContents(), true);

        return $pf_price;
    }

    public function premiumPriceEdit($token, $pricing_id, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;

        $response = $client->request('PUT', config('constants.api_url').'/premium/price/list/'.$pricing_id.'/update' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $pf_price = json_decode($response->getBody()->getContents(), true);

        return $pf_price;
    }

    public function premiumPriceStore($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('POST', config('constants.api_url').'/premiums/pricing/create' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $pf_price = json_decode($response->getBody()->getContents(), true);

        return $pf_price;
    }

    public function premiumShopIndex($token, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        $response = $client->request('GET', config('constants.api_url').'/premium/shop/list' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $premiums = json_decode($response->getBody()->getContents(), true);

        return $premiums;
    }

    public function premiumShopSearch($token, $page, $keyword)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        $response = $client->request('GET', config('constants.api_url').'/premium/shop/search?keyword='.$keyword , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $premiums = json_decode($response->getBody()->getContents(), true);

        return $premiums;
    }

    public function premiumPriceDelete($token, $id)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];

        $response = $client->request('DELETE', config('constants.api_url').'/premium/price/'.$id.'/delete' , [
            'headers' => $headers, 
        ]);
        $premiums = json_decode($response->getBody()->getContents(), true);

        return $premiums;
    }
}