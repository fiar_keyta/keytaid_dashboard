<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Voucher
{   
    public function blastVoucher($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('POST', config('constants.api_url')."/voucher/blast" , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
    }

    public function shopVoucher($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('POST', config('constants.api_url')."/voucher/create" , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
    }

    public function queueVoucher($token, $data)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = $data;
        $response = $client->request('POST', config('constants.api_url')."/voucher/queue" , [
            'headers' => $headers, 
            'multipart' => $body, 
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
    }

    public function voucherTitleLists($token)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $response = $client->request('GET', config('constants.api_url')."/voucher/title" , [
            'headers' => $headers,
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
        return $voucher;
    }

    public function voucherByTitle($token, $voucher_title, $page, $filter)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];
        
        if(isset($filter)){
            $adder = ['filter' => $filter];
            $body = $body + $adder;
        }
        $response = $client->request('GET', config('constants.api_url')."/voucher/by/title?voucher_title={$voucher_title}" , [
            'headers' => $headers,
            'json' => $body,
        ]);
        $voucher = json_decode($response->getBody()->getContents(), true);
        return $voucher;
    }
}