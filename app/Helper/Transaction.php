<?php
namespace App\Helper;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Transaction
{
    public function transactionsList($token, $page, $channel, $expedition_name, $payment_method, $status, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'page' => $page, 
            'per_page' => 25
        ];

        if(isset($channel)){
            $adder = ['channel' => $channel];
            $body = $body + $adder;
        }
        if(isset($date_range)){
            $adder = ['date_range' => $date_range];
            $body = $body + $adder;
        }
        if(isset($expedition_name)){
            $adder = ['expedition_name' => $expedition_name];
            $body = $body + $adder;
        }
        if(isset($payment_method)){
            $adder = ['payment_method' => $payment_method];
            $body = $body + $adder;
        }
        if(isset($status)){
            $adder = ['status' => $status];
            $body = $body + $adder;
        }
        // dd($body);
        $response = $client->request('GET', config('constants.api_url').'/transactions' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $transactions = json_decode($response->getBody()->getContents(), true);

        // dd($transactions);
        return $transactions;
    }

    public function transactionsExport($token, $date_range)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [ 
            'date_range' => $date_range,
        ];
        $response = $client->request('GET', config('constants.api_url').'/transactions/export' , [
            'headers' => $headers,
            'json' => $body,
        ]);
        $transactions = json_decode($response->getBody()->getContents(), true);

        return $transactions;
    }

    public function transactionsSearch($token, $keyword, $page)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Authorization' => "Bearer {$token}" ];
        $body = [
            'keyword' => $keyword, 
            'page' => $page, 
            'per_page' => 25
        ];
        $response = $client->request('GET', config('constants.api_url').'/transactions/all/search' , [
            'headers' => $headers, 
            'json' => $body,
        ]);
        $transactions = json_decode($response->getBody()->getContents(), true);

        return $transactions;
    }

    public function requestDownload($date_range, $whatsapp)
    {
        $client = new \GuzzleHttp\Client();
        $body = [
            'date_range' => $date_range, 
            'phone' => $whatsapp,
            'table' => 'invoices',
        ];
        $response = $client->request('POST', 'https://report.keyta.id/request-export' , [
            'json' => $body,
        ]);
        $transactions = json_decode($response->getBody()->getContents(), true);

        return $transactions;
    }
}