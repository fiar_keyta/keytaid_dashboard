<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Cost;

class CostsExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $token = Helper::getToken();
        $costs = Cost::costsExport($token);
        $i = 0;
        foreach ($costs['results'] as $cost) {
            $result[$i] = [
                'Nama Toko' => $cost['shop']['name'],
                'Origin Address' => $cost['origin_detail'],
                'Origin Latitude' => $cost['origin_latitude'],
                'Origin Longitude' => $cost['origin_longitude'],
                'Origin Postcode' => $cost['origin_postcode'],
                'Destination Address' => $cost['dest_detail'],
                'Destination Latitude' => $cost['dest_latitude'],
                'Destination Longitude' => $cost['dest_longitude'],
                'Destination Postcode' => $cost['dest_postcode'],
                'Nama Ekspedition' => $cost['courier'],
                'Partner' => $cost['partner']
            ];
            
            $i++;
        }
        
        return $result;
    }

    public function headings(): array
    { 
        return [
                "Nama Toko",
                "Origin Address",
                "Origin Latitude",
                "Origin Longitude",
                "Origin Postcode",
                "Destination Address",
                "Destination Latitude",
                "Destination Longitude",
                "Destination Postcode",
                "Nama Ekspedition",
                "Partner"
            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_NUMBER,
            'D' => NumberFormat::FORMAT_NUMBER,
            'E' => NumberFormat::FORMAT_NUMBER,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER,
            'I' => NumberFormat::FORMAT_NUMBER,
            'J' => NumberFormat::FORMAT_TEXT,
            'K' => NumberFormat::FORMAT_TEXT,
        ];
    }
}
