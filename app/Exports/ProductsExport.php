<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Product;

class ProductsExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $token = Helper::getToken();
        $products = Product::productsExport($token);
        $i = 0;
        foreach ($products['results'] as $product) {
            if(isset($product['shop'])){
                $result[$i] = [
                    'Nama Toko' => $product['shop']['name'],
                    'Nama Produk' => $product['name'],
                    'Harga Produk' => $product['price'],
                    'Diskon' => $product['discount'],
                    'Length' => $product['length'],
                    'Width' => $product['width'],
                    'Height' => $product['height'],
                    'Weight' => $product['height']
                ];
            }
            
            $i++;
        }
        
        return $result;
    }

    public function headings(): array
    { 
        return [
                'Nama Toko',
                'Nama Produk',
                'Harga Produk',
                'Diskon',
                'Length',
                'Width',
                'Height',
                'Weight'
            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_NUMBER,
            'D' => NumberFormat::FORMAT_NUMBER,
            'E' => NumberFormat::FORMAT_NUMBER,
            'F' => NumberFormat::FORMAT_NUMBER,
            'G' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER
        ];
    }
}
