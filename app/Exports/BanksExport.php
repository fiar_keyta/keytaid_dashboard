<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\BankAccount;

class BanksExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $token = Helper::getToken();
        $banks = BankAccount::bankAccountExport($token);
        $i = 0;
        foreach ($banks['results'] as $bank) {
            if(isset($bank['shop'])){
                $result[$i] = [
                    'Nama Toko' => $bank['shop']['name'],
                    'ID Toko' => $bank['shop']['id'],
                    'Nama Bank' => $bank['name'],
                    'Nama Pemilik' => $bank['account_name'],
                    'Nomor Rekening' => "{$bank['account_number']}"
                ];
            }
            else {
                $result[$i] = [
                    'Nama Toko' => '',
                    'ID Toko' => '',
                    'Nama Bank' => $bank['name'],
                    'Nama Pemilik' => $bank['account_name'],
                    'Nomor Rekening' => "{$bank['account_number']}"
                ];
            }
            
            $i++;
        }
        
        return $result;
    }

    public function headings(): array
    { 
        return [
                'Nama Toko',
                'ID Toko',
                'Nama Bank',
                'Nama Pemilik Rekening',
                'Nomor Rekening'
            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_NUMBER
        ];
    }
}
