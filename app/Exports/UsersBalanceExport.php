<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Keytauser;

class UsersBalanceExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $shop_id_range;

    function __construct(string $shop_id_range) {
        $this->shop_id_range = $shop_id_range;
    }
    public function array(): array
    {
        $token = Helper::getToken();

        $usersBalance = Keytauser::userBalanceExport($token, $this->shop_id_range);
        $i = 0;

        foreach ($usersBalance['results'] as $userBalance) {
            if(isset($userBalance['histories'][0])){
                $result[$i] = [
                    'Point ID' => $userBalance['id'],
                    'Shop ID' => $userBalance['shop_id'],
                    'Shop Name' => $userBalance['shop']['name'],
                    'Last Action' => $userBalance['histories'][0]['payment_type'],
                    'Amount' => $userBalance['histories'][0]['amount'],
                    'Balance' => $userBalance['readable_balance']
                ];
            }
            else {
                $result[$i] = [
                    'Point ID' => $userBalance['id'],
                    'Shop ID' => $userBalance['shop_id'],
                    'Shop Name' => $userBalance['shop']['name'],
                    'Last Action' => "-",
                    'Amount' => "-",
                    'Balance' => $userBalance['readable_balance']
                ];
            }
            
            $i++;
        }
        
        return $result;
    }
    
    public function headings(): array
    { 
        return [
            'Point ID',
            'Shop ID',
            'Shop Name',
            'Last Action',
            'Amount',
            'Balance'
            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'B' => NumberFormat::FORMAT_NUMBER,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_NUMBER,
            'F' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
