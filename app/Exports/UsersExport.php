<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Keytauser;

class UsersExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        $token = Helper::getToken();
        $users = Keytauser::userExport($token);
        $i = 0;

        foreach ($users['results'] as $user) {
            if(isset($user['shop'])){
                $result[$i] = [
                    'id' => $user['id'],
                    'Tanggal Registrasi' => $user['created_at'],
                    'Nomor HP' => $user['phone_with_code'],
                    'Nama' => $user['name'],
                    'Nama Toko' => $user['shop']['name'],
                    'Alamat Toko' => $user['shop']['address']
                ];
            }
            else {
                $result[$i] = [
                    'id' => $user['id'],
                    'Tanggal Registrasi' => $user['created_at'],
                    'Nomor HP' => $user['phone_with_code'],
                    'Nama' => $user['name']
                ];
            }
            
            $i++;
        }
        
        return $result;
    }

    public function headings(): array
    { 
        return [
            'id',
            'Tanggal Registrasi',
            'Nomor HP',
            'Nama',
            'Nama Toko',
            'Alamat Toko'
            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_NUMBER,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT
        ];
    }
}
