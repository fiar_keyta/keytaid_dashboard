<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Premium;

class OrderPremiumExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $date_range;

    function __construct(string $date_range) {
        $this->date_range = $date_range;
    }

    public function array(): array
    {
        $token = Helper::getToken();
        $orders = Premium::ordersExport($token, $this->date_range);
        $i = 0;
        $x = 0;
        $last_id = 0;
        foreach ($orders['results'] as $order) {
            if($order['ewallet_payment'] > 0 && $order['keytapoint_payment'] > 0){
                $payment_method = "KEYTAPOINT + ".$order['premium_payments']['ewallet_type'];
            }
            elseif($order['premium_payments']['external_id'] == null){
                $payment_method = "Trial";
            }
            else{
                $payment_method = $order['premium_payments']['ewallet_type'];
            }
            
            $result[$x] = [
                'Tanggal Order' => str_replace("T"," ",substr($order['created_at'], 0, 19)),
                'Shop Id' => $order['shops']['id'],
                'Nama Toko' => $order['shops']['name'],
                'Feature' => $order['details']['premium_feature'],
                'Duration' => $order['details']['premium_duration'],
                'Quantity' => $order['details']['premium_quantity'],
                'Start' => $order['details']['start_date'],
                'Expired' => $order['details']['expired_date'],
                'Status' => $order['status'],
                'Amount' => $order['details']['premium_pricing'],
                'Total Amount (after disc)' => $order['amount'],
                'Keytapoint' => $order['keytapoint_payment'],
                'Ewallet' => $order['ewallet_payment'],
                'Payment Method' => $payment_method,
                'Reference Id' => $order['premium_payments']['external_id']
            ];
            $x++;
            
        }
        
        return $result;
    }

    public function headings(): array
    { 
        return [
                'Tanggal Order',
                'Shop Id',
                'Nama Toko',
                'Feature',
                'Duration',
                'Quantity',
                'Start',
                'Expired',
                'Status',
                'Amount',
                'Total Amount (after disc)',
                'Keytapoint',
                'Ewallet',
                'Payment Method',
                'Reference Id'
            ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_DATE_DATETIME,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_DATE_YYYYMMDD,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_NUMBER,
            'G' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER,
            'I' => NumberFormat::FORMAT_TEXT,
            'J' => NumberFormat::FORMAT_TEXT
        ];
    }
}
