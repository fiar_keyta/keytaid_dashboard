<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Keytapoint;

class KeytaPointHistoryExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $date_range;

    function __construct(string $date_range) {
        $this->date_range = $date_range;
    }

    public function array(): array
    {
        $token = Helper::getToken();

        $historyPoints = Keytapoint::pointHistoryExport($token,  $this->date_range);
        $i = 0;

        foreach ($historyPoints['results'] as $historyPoint) {
            if(isset($historyPoint['shop'])){
                $result[$i] = [
                    'Status' => "Completed",
                    'Date Created' => $historyPoint['created_at'],
                    'Shop ID' => $historyPoint['shop']['id'],
                    'Shop Name' => $historyPoint['shop']['name'],
                    'Transaction Number' => $historyPoint['number'],
                    'Transaction Type' => $historyPoint['payment_type'],
                    'Reference' => $historyPoint['id'],
                    'Amount' => $historyPoint['amount'],
                    'Balance' => $historyPoint['keyta_balance']
                ];
            } else {
                $result[$i] = [
                    'Status' => "Completed",
                    'Date Created' => $historyPoint['created_at'],
                    'Shop ID' => '-',
                    'Shop Name' => '-',
                    'Transaction Number' => $historyPoint['number'],
                    'Transaction Type' => $historyPoint['payment_type'],
                    'Reference' => $historyPoint['id'],
                    'Amount' => $historyPoint['amount'],
                    'Balance' => $historyPoint['keyta_balance']
                ];
            }
            $i++;
        }
        
        return $result;
    }
    
    public function headings(): array
    { 
        return [
            'Status',
            'Date Created',
            'Shop ID',
            'Shop Name',
            'Transaction Number',
            'Transaction Type',
            'Reference',
            'Amount',
            'Balance'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_NUMBER,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER,
            'I' => NumberFormat::FORMAT_NUMBER
        ];
    }
}
