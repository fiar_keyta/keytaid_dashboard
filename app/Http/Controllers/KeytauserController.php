<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Canvaser;
use Facades\App\Helper\Keytauser;
use App\Exports\UsersExport;
use App\Exports\UsersCanvaserExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class KeytauserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role_access:user');
        // $this->middleware('role_access:admin');
    }

    public function index(Request $request, $id, $page)
	{
        $sort_by = $request->get('sort_by');
        $sort_table_name = $request->get('sort_table_name');
        $sort_column_name = $request->get('sort_column_name');
        $community_name = $request->get('community_name');
        $date_range= $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $dashboard = Keytauser::dashboard($token);
        $users = Keytauser::userList($token, $page, $community_name, $sort_by, $sort_table_name, $sort_column_name, $date_range);

        $communities = Keytauser::communityList($token);
        $data=[
            'title' => 'Keyta Users',
            'user_id'=> $id,
            'access_level' => $access_level,
            'users' => $users,
            'community_name' => $community_name,
            'community_lists' => $communities,
            'dashboard' => $dashboard,
            'sort_by' => $sort_by,
            'sort_table_name' => $sort_table_name,
            'sort_column_name' => $sort_column_name,
            'date_range' => $date_range
        ];

    	return view('admin/admin/keytauser/list')->with($data);
    }
    
    public function canvaser_user_list(Request $request, $id, $page)
	{
        $access_level = \App\User::find($id);
        $date_range= $request->get('date_range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $keyword = $request->get('keyword');
        $referrer = $request->get('referrer');
        if (!$referrer) {
            $referrer = $access_level["referral_code"];
        }
        $token = Helper::getToken();
        $users = Canvaser::userList($token, $page, $access_level["username"], $access_level["shop_id"], $referrer, $access_level["tag"], $keyword, $date_range);
        $data=[
            'title' => 'Keyta Users',
            'user_id'=> $id,
            'access_level' => $access_level,
            'users' => $users,
            'date_range' => $date_range,
            'referrer' => $referrer,
            'keyword' => $keyword
        ];
    	return view('admin/admin/keytauser/list')->with($data);
    }

    public function canvaser_user_export(Request $request, $id)
	{
        return Excel::download(new UsersCanvaserExport($id), 'Canvasers.xlsx');
    }

    public function search(Request $request, $id, $page)
    {
        $keyword = $request->get('keyword');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $users = Keytauser::userSearch($token, $keyword, $page);
        $data=[
            'title' => 'Keyta Users',
            'user_id'=> $id,
            'access_level' => $access_level,
            'users' => $users,
            'keyword' => $keyword
        ];
    	return view('admin/admin/keytauser/list_search')->with($data);
    }

    public function export()
    {
        return Excel::download(new UsersExport(), 'Users.xlsx');
    }

    
}
