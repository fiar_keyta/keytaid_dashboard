<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Faq;
use Redirect;
use Session;

class FaqsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index($id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $faqs = Faq::faqList($token);
        $data=[
            'title' => 'FAQ',
            'user_id'=> $id,
            'access_level' => $access_level,
            'faqs' => $faqs
        ];
    	return view('admin/admin/faq/list')->with($data);
    }

    public function edit($id, $faq_id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $faq = Faq::faqById($token, $faq_id);
        $data=[
            'title' => 'FAQ',
            'user_id'=> $id,
            'access_level' => $access_level,
            'faq' => $faq
        ];
    	return view('admin/admin/faq/edit')->with($data);
    }

    public function update(Request $request, $id, $faq_id)
	{
        $question = $request->post("question");
        $answer = $request->post("answer");
        $category = $request->post("category");
        $subcategory = $request->post("subcategory");
        $category_ordering = $request->post("category_ordering");
        $subcategory_ordering = $request->post("subcategory_ordering");

        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $faq = Faq::faqUpdate($token, $faq_id, $question, $answer, $category, $subcategory, $category_ordering, $subcategory_ordering);
        $data=[
            'title' => 'FAQ',
            'user_id'=> $id,
            'access_level' => $access_level,
            'faq' => $faq
        ];
    	return redirect('/admin/'.$id.'/app-management/faq')->with(['message' => 'Edit Berhasil']);
    }

    public function add($id)
	{
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'FAQ',
            'user_id'=> $id,
            'access_level' => $access_level
        ];
    	return view('admin/admin/faq/add')->with($data);
    }

    public function store(Request $request, $id)
	{
        $question = $request->post("question");
        $answer = $request->post("answer");
        $category = $request->post("category");
        $subcategory = $request->post("subcategory");
        $category_ordering = $request->post("category_ordering");
        $subcategory_ordering = $request->post("subcategory_ordering");

        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $faq = Faq::faqAdd($token, $question, $answer, $category, $subcategory, $category_ordering, $subcategory_ordering);
        $data=[
            'title' => 'FAQ',
            'user_id'=> $id,
            'access_level' => $access_level,
            'faq' => $faq
        ];
    	return redirect('/admin/'.$id.'/app-management/faq')->with(['message' => 'Tambah FAQ Berhasil']);
    }
}

