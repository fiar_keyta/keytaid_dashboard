<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Voucher;
use Facades\App\Helper\Expedition;
use App\Exports\TransactionsExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class VoucherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index($id) 
	{
        $token = Helper::getToken();
        $expeditions = Expedition::list($token);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher Blast',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
    	return view('admin/admin/voucher/list')->with($data);
    }

    public function shop_index($id) 
	{
        $token = Helper::getToken();
        $expeditions = Expedition::list($token);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher by Shop',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
    	return view('admin/admin/voucher/shop-list')->with($data);
    }

    public function queue_index($id) 
	{
        $token = Helper::getToken();
        $expeditions = Expedition::list($token);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher Queues',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
    	return view('admin/admin/voucher/queue-list')->with($data);
    }

    public function blast_voucher(Request $request, $id)
    {
        $voucher_type = $request->post('voucher_type');

        $checkbox_expd = $request->post('expeditions');
        $expedition_id = implode(",",$checkbox_expd);
        $expedition_ids = "[".$expedition_id."]";

        $checkbox_pay = $request->post('payment_method');
        $payments = implode(",",$checkbox_pay);
        $payment_method = "[".$payments."]";

        $voucher_title = $request->post('voucher_title');
        $voucher_value = $request->post('voucher_value');
        $code = $request->post('code');
        $unit = $request->post('unit');
        $max_value = $request->post('max_value');
        $date_range = str_replace(' ', '', $request->post('date_range'));
        $promo_template = $request->post('promo_template');
        $description = $request->post('description');
        $voucher_image = $request->file('voucher_image');
        $voucher_logo = $request->file('voucher_logo');
        $quantity = $request->post('voucher_by_user_qty');
        $max_shipping_price = $request->post('max_shipping_price');
        $min_shipping_price = $request->post('min_shipping_price');

        $dates = $pieces = explode("-", $date_range);
        $start_date = date("Y-m-d", strtotime($dates[0]."-".$dates[1]."-".$dates[2]));
        $expired_date = date("Y-m-d", strtotime($dates[3]."-".$dates[4]."-".$dates[5]));

        if($payment_method == ""){
            $data = [
                [
                    'name' => 'shop_id',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'expedition_ids',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'voucher_title',
                    'contents' => $voucher_title,
                ],
                [
                    'name' => 'code',
                    'contents' => $code,
                ],
                [
                    'name' => 'description',
                    'contents' => $description,
                ],
                [
                    'name' => 'value',
                    'contents' => $voucher_value,
                ],
                [
                    'name' => 'unit',
                    'contents' => strtolower($unit),
                ],
                [
                    'name' => 'voucher_type',
                    'contents' => strtolower($voucher_type),
                ],
                [
                    'name' => 'start_date',
                    'contents' => $start_date,
                ],
                [
                    'name' => 'expired_date',
                    'contents' => $expired_date,
                ],
                [
                    'name' => 'voucher_by_user_qty',
                    'contents' => $quantity,
                ],
                [
                    'name' => 'promo_template',
                    'contents' => $promo_template,
                ],
                [
                    'name' => 'max_value',
                    'contents' => $max_value,
                ],
                [
                    'name' => 'max_shipping_price',
                    'contents' => $max_shipping_price
                ],
                [
                    'name' => 'min_shipping_price',
                    'contents' => $min_shipping_price
                ],
                [
                    'name'     => 'voucher_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_image'
                ],
                [
                    'name'     => 'voucher_promo_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_logo'
                ]
            ];
        } else {
            $data = [
                [
                    'name' => 'shop_id',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'expedition_ids',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'payment_method',
                    'contents' => $payment_method,
                ],
                [
                    'name' => 'voucher_title',
                    'contents' => $voucher_title,
                ],
                [
                    'name' => 'code',
                    'contents' => $code,
                ],
                [
                    'name' => 'description',
                    'contents' => $description,
                ],
                [
                    'name' => 'value',
                    'contents' => $voucher_value,
                ],
                [
                    'name' => 'unit',
                    'contents' => strtolower($unit),
                ],
                [
                    'name' => 'voucher_type',
                    'contents' => strtolower($voucher_type),
                ],
                [
                    'name' => 'start_date',
                    'contents' => $start_date,
                ],
                [
                    'name' => 'expired_date',
                    'contents' => $expired_date,
                ],
                [
                    'name' => 'voucher_by_user_qty',
                    'contents' => $quantity,
                ],
                [
                    'name' => 'promo_template',
                    'contents' => $promo_template,
                ],
                [
                    'name' => 'max_value',
                    'contents' => $max_value,
                ],
                [
                    'name' => 'max_shipping_price',
                    'contents' => $max_shipping_price
                ],
                [
                    'name' => 'min_shipping_price',
                    'contents' => $min_shipping_price
                ],
                [
                    'name'     => 'voucher_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_image'
                ],
                [
                    'name'     => 'voucher_promo_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_logo'
                ]
            ];
        }

        set_time_limit(1200);
        
        $token = Helper::getToken();
        $blast = Voucher::blastVoucher($token, $data);

    	return redirect()->route('voucher_blast', ['id' => $id])->with(['message' => 'Blast Voucher Success']);
    }

    public function shop_voucher(Request $request, $id)
    {
        $shop_id = $request->post('shop_id');
        $voucher_type = $request->post('voucher_type');

        $checkbox_expd = $request->post('expeditions');
        $expedition_id = implode(",",$checkbox_expd);
        $expedition_ids = "[".$expedition_id."]";

        $checkbox_pay = $request->post('payment_method');
        $payments = implode(",",$checkbox_pay);
        $payment_method = "[".$payments."]";
        
        $voucher_title = $request->post('voucher_title');
        $code = $request->post('code');
        $voucher_value = $request->post('voucher_value');
        $unit = $request->post('unit');
        $max_value = $request->post('max_value');
        $date_range = str_replace(' ', '', $request->post('date_range'));
        $promo_template = $request->post('promo_template');
        $description = $request->post('description');
        $voucher_image = $request->file('voucher_image');
        $voucher_logo = $request->file('voucher_logo');
        $quantity = $request->post('voucher_by_user_qty');
        $max_shipping_price = $request->post('max_shipping_price');
        $min_shipping_price = $request->post('min_shipping_price');

        $dates = $pieces = explode("-", $date_range);
        $start_date = date("Y-m-d", strtotime($dates[0]."-".$dates[1]."-".$dates[2]));
        $expired_date = date("Y-m-d", strtotime($dates[3]."-".$dates[4]."-".$dates[5]));

        if($payment_method == ""){
            $data = [
                [
                    'name' => 'shop_id',
                    'contents' => $shop_id,
                ],
                [
                    'name' => 'expedition_ids',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'voucher_title',
                    'contents' => $voucher_title,
                ],
                [
                    'name' => 'code',
                    'contents' => $code,
                ],
                [
                    'name' => 'description',
                    'contents' => $description,
                ],
                [
                    'name' => 'value',
                    'contents' => $voucher_value,
                ],
                [
                    'name' => 'unit',
                    'contents' => strtolower($unit),
                ],
                [
                    'name' => 'voucher_type',
                    'contents' => strtolower($voucher_type),
                ],
                [
                    'name' => 'start_date',
                    'contents' => $start_date,
                ],
                [
                    'name' => 'expired_date',
                    'contents' => $expired_date,
                ],
                [
                    'name' => 'voucher_by_user_qty',
                    'contents' => $quantity,
                ],
                [
                    'name' => 'promo_template',
                    'contents' => $promo_template,
                ],
                [
                    'name' => 'max_value',
                    'contents' => $max_value,
                ],
                [
                    'name' => 'max_shipping_price',
                    'contents' => $max_shipping_price
                ],
                [
                    'name' => 'min_shipping_price',
                    'contents' => $min_shipping_price
                ],
                [
                    'name'     => 'voucher_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_image'
                ],
                [
                    'name'     => 'voucher_promo_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_logo'
                ]
            ];
        } else {
            $data = [
                [
                    'name' => 'shop_id',
                    'contents' => $shop_id,
                ],
                [
                    'name' => 'expedition_ids',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'payment_method',
                    'contents' => $payment_method,
                ],
                [
                    'name' => 'voucher_title',
                    'contents' => $voucher_title,
                ],
                [
                    'name' => 'code',
                    'contents' => $code,
                ],
                [
                    'name' => 'description',
                    'contents' => $description,
                ],
                [
                    'name' => 'value',
                    'contents' => $voucher_value,
                ],
                [
                    'name' => 'unit',
                    'contents' => strtolower($unit),
                ],
                [
                    'name' => 'voucher_type',
                    'contents' => strtolower($voucher_type),
                ],
                [
                    'name' => 'start_date',
                    'contents' => $start_date,
                ],
                [
                    'name' => 'expired_date',
                    'contents' => $expired_date,
                ],
                [
                    'name' => 'voucher_by_user_qty',
                    'contents' => $quantity,
                ],
                [
                    'name' => 'promo_template',
                    'contents' => $promo_template,
                ],
                [
                    'name' => 'max_value',
                    'contents' => $max_value,
                ],
                [
                    'name' => 'max_shipping_price',
                    'contents' => $max_shipping_price
                ],
                [
                    'name' => 'min_shipping_price',
                    'contents' => $min_shipping_price
                ],
                [
                    'name'     => 'voucher_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_image'
                ],
                [
                    'name'     => 'voucher_promo_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_logo'
                ]
            ];
        }
        
        $token = Helper::getToken();
        $blast = Voucher::shopVoucher($token, $data);

    	return redirect()->route('voucher_shop', ['id' => $id])->with(['message' => 'Give Voucher Success']);
    }

    public function queue_voucher(Request $request, $id)
    {
        $voucher_type = $request->post('voucher_type');

        $checkbox_expd = $request->post('expeditions');
        $expedition_id = implode(",",$checkbox_expd);
        $expedition_ids = "[".$expedition_id."]";

        $checkbox_pay = $request->post('payment_method');
        $payments = implode(",",$checkbox_pay);
        $payment_method = "[".$payments."]";
        
        $voucher_title = $request->post('voucher_title');
        $code = $request->post('code');
        $voucher_value = $request->post('voucher_value');
        $unit = $request->post('unit');
        $max_value = $request->post('max_value');
        $date_range = str_replace(' ', '', $request->post('date_range'));
        $promo_template = $request->post('promo_template');
        $description = $request->post('description');
        $voucher_image = $request->file('voucher_image');
        $voucher_logo = $request->file('voucher_logo');
        $quantity = $request->post('quantity');
        $max_shipping_price = $request->post('max_shipping_price');
        $min_shipping_price = $request->post('min_shipping_price');
        $voucher_expired_in_days = $request->post('voucher_expired_in_days');

        $dates = $pieces = explode("-", $date_range);
        $start_date = date("Y-m-d", strtotime($dates[0]."-".$dates[1]."-".$dates[2]));
        $expired_date = date("Y-m-d", strtotime($dates[3]."-".$dates[4]."-".$dates[5]));

        if($payment_method == ""){
            $data = [
                [
                    'name' => 'expedition_ids',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'voucher_title',
                    'contents' => $voucher_title,
                ],
                [
                    'name' => 'code',
                    'contents' => $code,
                ],
                [
                    'name' => 'description',
                    'contents' => $description,
                ],
                [
                    'name' => 'value',
                    'contents' => $voucher_value,
                ],
                [
                    'name' => 'unit',
                    'contents' => strtolower($unit),
                ],
                [
                    'name' => 'voucher_type',
                    'contents' => strtolower($voucher_type),
                ],
                [
                    'name' => 'queue_start_date',
                    'contents' => $start_date,
                ],
                [
                    'name' => 'queue_expired_date',
                    'contents' => $expired_date,
                ],
                [
                    'name' => 'quantity',
                    'contents' => $quantity,
                ],
                [
                    'name' => 'promo_template',
                    'contents' => $promo_template,
                ],
                [
                    'name' => 'max_value',
                    'contents' => $max_value,
                ],
                [
                    'name' => 'voucher_expired_in_days',
                    'contents' => $voucher_expired_in_days,
                ],
                [
                    'name' => 'max_shipping_price',
                    'contents' => $max_shipping_price
                ],
                [
                    'name' => 'min_shipping_price',
                    'contents' => $min_shipping_price
                ],
                [
                    'name'     => 'voucher_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_image'
                ],
                [
                    'name'     => 'voucher_promo_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_logo'
                ]
            ];
        } else {
            $data = [
                [
                    'name' => 'expedition_ids',
                    'contents' => $expedition_ids,
                ],
                [
                    'name' => 'payment_method',
                    'contents' => $payment_method,
                ],
                [
                    'name' => 'voucher_title',
                    'contents' => $voucher_title,
                ],
                [
                    'name' => 'code',
                    'contents' => $code,
                ],
                [
                    'name' => 'description',
                    'contents' => $description,
                ],
                [
                    'name' => 'value',
                    'contents' => $voucher_value,
                ],
                [
                    'name' => 'unit',
                    'contents' => strtolower($unit),
                ],
                [
                    'name' => 'voucher_type',
                    'contents' => strtolower($voucher_type),
                ],
                [
                    'name' => 'queue_start_date',
                    'contents' => $start_date,
                ],
                [
                    'name' => 'queue_expired_date',
                    'contents' => $expired_date,
                ],
                [
                    'name' => 'quantity',
                    'contents' => $quantity,
                ],
                [
                    'name' => 'promo_template',
                    'contents' => $promo_template,
                ],
                [
                    'name' => 'max_value',
                    'contents' => $max_value,
                ],
                [
                    'name' => 'voucher_expired_in_days',
                    'contents' => $voucher_expired_in_days,
                ],
                [
                    'name' => 'max_shipping_price',
                    'contents' => $max_shipping_price
                ],
                [
                    'name' => 'min_shipping_price',
                    'contents' => $min_shipping_price
                ],
                [
                    'name'     => 'voucher_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_image'
                ],
                [
                    'name'     => 'voucher_promo_image',
                    'contents' => file_get_contents($voucher_image),
                    'filename' => 'voucher_logo'
                ]
            ];
        }
        
        $token = Helper::getToken();
        $blast = Voucher::queueVoucher($token, $data);

    	return redirect()->route('voucher_queue', ['id' => $id])->with(['message' => 'Give Voucher Success']);
    }

    public function list($id) 
	{
        $token = Helper::getToken();
        $vouchers = Voucher::voucherTitleLists($token);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher List',
            'user_id'=> $id,
            'access_level' => $access_level,
            'vouchers' => $vouchers
        ];
    	return view('admin/admin/voucher_list/list')->with($data);
    }

    public function list_by_title(Request $request, $id, $voucher_title, $page) 
	{
        $filter = $request->post('filter');
        $token = Helper::getToken();
        $vouchers = Voucher::voucherByTitle($token, $voucher_title, $page, $filter);
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'Voucher List',
            'user_id'=> $id,
            'access_level' => $access_level,
            'vouchers' => $vouchers,
            'voucher_title' => $voucher_title,
            'filter' => $filter
        ];
    	return view('admin/admin/voucher_list/details')->with($data);
    }
}
