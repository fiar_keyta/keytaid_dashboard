<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use DB;

class SuperuserController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role_access:superuser');
	}
    

	public function show($id)
	{
		$users = \App\User::all();
        $access_level = \App\User::find($id);
        $data=[
            'title' => 'User List',
            'user_id'=> $id,
            'access_level' => $access_level,
            'users' => $users
        ];
    	return view('admin/superuser/userList')->with($data);
	}

    public function getRegister()
    {
    	return view('admin/superuser/registerSuperuser');
    }

    public function postRegister()
    {
    	$rules=[
    		'username' => 'required',
    		'email' => 'required',
    		'name' => 'required',
    		'password' => 'required|min:6|confirmed',
    		'term' => 'required'

    	];

    	//Pesan Validasi
    	$message=[
    		'username.required'=>'Username Require',
    		'name.required'=>'Name Require',
    		'password.required'=>'Password Require',
    		'password.min'=>'Password Min 6 Character',
    		'password.confirmed'=>'Password not match',
    		'email.required'=>'Email Require',
    		'term.required' => 'Please Accept Term & Condition'
    	];

    	$validator=Validator::make(Input::all(),$rules,$message);

    	if($validator->fails()){
    		return Redirect::to('superuser/register')->withErrors($validator);
    	}else{

    	$user = new \App\User; //manggil model katanyamah
    	$user->username = Input::get('username');
    	$user->email = Input::get('email');
    	$user->name = Input::get('name');
    	$user->password = bcrypt(Input::get('password'));
    	$user->remember_token = Input::get('_token');
    	$user->role_id = DB::table('roles')->select('id')->where('access_level','superuser')->first()->id;
        $user->save();



    	Session::flash('message','Register Success');
    	return Redirect::to('login');
    	}

    }

    public function getAdminRegister($id)
    {
    	$access_level = \App\User::find($id);
        $data=[
            'title' => 'Tambah Admin',
            'user_id'=> $id,
            'access_level' => $access_level
        ];
    	return view('admin/superuser/registerAdmin')->with($data);
    }

    public function postAdmin($id)
    {
    	$rules=[
    		'username' => 'required',
    		'email' => 'required',
    		'name' => 'required',
    		'password' => 'required|min:6|confirmed'

    	];

    	//Pesan Validasi
    	$message=[
    		'username.required'=>'Username Require',
    		'name.required'=>'Name Require',
    		'password.required'=>'Password Require',
    		'password.min'=>'Password Min 6 Character',
    		'password.confirmed'=>'Password not match',
    		'email.required'=>'Email Require'
    	];

    	$validator=Validator::make(Input::all(),$rules,$message);

    	if($validator->fails()){
    		return Redirect::to('superuser/tambahadmin/'.$id)->withErrors($validator);
    	}else{

    	$user = new \App\User; //manggil model katanyamah
    	$user->username = Input::get('username');
    	$user->email = Input::get('email');
    	$user->name = Input::get('name');
    	$user->password = bcrypt(Input::get('password'));
    	$user->remember_token = Input::get('_token');
    	$user->role_id = DB::table('roles')->select('id')->where('access_level','admin')->first()->id;
        $user->save();



    	Session::flash('message','Register Admin Success');
    	return Redirect::to('superuser/'.$id);
    	}

    }
}
