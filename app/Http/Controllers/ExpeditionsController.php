<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Expedition;
use App\Exports\ExpeditionsExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class ExpeditionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page)
	{
        $filter = $request->get('filter');
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $expeditions = Expedition::expeditionsList($token, $page, $filter);
        $data=[
            'title' => 'Expeditions',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions,
            'filter' => $filter
        ];
    	return view('admin/admin/expedition/list')->with($data);
    }
    
    public function search(Request $request, $id, $page)
    {
        $filter = $request->get('filter');
        $keyword = $request->get('keyword');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $expeditions = Expedition::expeditionsSearch($token, $keyword, $page);
        $data=[
            'title' => 'Expeditions',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions,
            'keyword' => $keyword,
            'filter' => $filter
        ];
    	return view('admin/admin/expedition/list_search')->with($data);
    }

    public function export()
    {
        return Excel::download(new ExpeditionsExport(), 'Expeditions.xlsx');
    }

    public function list($id)
    {
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $expeditions = Expedition::list($token);
        $data=[
            'title' => 'Expedition List',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
    	return view('admin/admin/expedition/list2')->with($data);
    }

    public function listUpdate(Request $request, $id, $expedition_id)
    {
        $token = Helper::getToken();
        $payload = [
            'name' => $request->post("name"),
            'code' => $request->post("code"),
            'partner' => $request->post("partner"),
            'ordering' => $request->post("ordering"),
            'curent_waybill' => $request->post("curent_waybill"),
            'max_waybill' => $request->post("max_waybill")
            // 'is_enable' => $request->post("is_enable"),
        ];
        $expeditions = Expedition::listUpdate($token, $expedition_id, $payload);
    	return redirect('/admin/'.$id.'/expeditions/list')->with(['message' => 'Ubah Ekspedisi Berhasil']);
    }

    public function listEdit($id, $expedition_id)
    {
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $expeditions = Expedition::list_by_id($token, $expedition_id);

        $data=[
            'title' => 'Expedition List',
            'user_id'=> $id,
            'access_level' => $access_level,
            'expeditions' => $expeditions
        ];
    	return view('admin/admin/expedition/edit')->with($data);
    }
}
