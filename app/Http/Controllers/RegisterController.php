<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use DB;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }
    
    public function getRegister($id)
    {
         $data=[
			'title' => 'Daftar',
			'user_id' => $id
        ];
    	return view('register/formRegister')->with($data);
    }

    public function postRegister($id)
    {
		
    	$rules=[
    		'username' => 'required',
    		'email' => 'required',
    		'name' => 'required',
    		'password' => 'required|min:6|confirmed',
    		'term' => 'required'

    	];

    	//Pesan Validasi
    	$message=[
    		'username.required'=>'Username Require',
    		'name.required'=>'Name Require',
    		'password.required'=>'Password Require',
    		'password.min'=>'Password Min 6 Character',
    		'password.confirmed'=>'Password not match',
    		'email.required'=>'Email Require',
    		'term.required' => 'Please Accept Term & Condition'
    	];

    	$validator=Validator::make(Input::all(),$rules,$message);

    	if($validator->fails()){
    		return Redirect::to('register')->withErrors($validator);
    	}else{

        $tenant = new \App\Tenant;
        $tenant->full_name = Input::get('name');
		$tenant->email = Input::get('email');
		$tenant->website = Input::get('email');
        $tenant->save();

    	$user = new \App\User; //manggil model katanyamah
    	$user->username = Input::get('username');
    	$user->email = Input::get('email');
    	$user->name = Input::get('name');
    	$user->password = bcrypt(Input::get('password'));
    	$user->remember_token = Input::get('_token');
    	$user->role_id = DB::table('roles')->select('id')->where('access_level','user')->first()->id;
        $user->tenant_id = DB::table('tenants')->select('id')->where('email', Input::get('email'))->first()->id;
    	$user->save();



    	Session::flash('message','Register Success');
    	return Redirect::to('/admin/daftartenan/'.$id);
    }

    }
}
