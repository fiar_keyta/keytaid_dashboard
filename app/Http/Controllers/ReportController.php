<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Report;
use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }
 
    public function index(Request $request,$id, $page) 
	{
        $status = $request->get('status');
        $payment_method = $request->get('payment_method');
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $reports = Report::reportsList($token, $page);
        $data=[
            'title' => 'Reports',
            'user_id'=> $id,
            'access_level' => $access_level,
            'reports' => $reports,
            'status' => $status,
            'payment_method' => $payment_method
        ];
    	return view('admin/admin/report/list')->with($data);
    }
    
    public function search(Request $request, $id, $page)
    {
        $keyword = $request->get('keyword');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $reports = Report::reportsSearch($token, $keyword, $page);
        $data=[
            'title' => 'Reports',
            'user_id'=> $id,
            'access_level' => $access_level,
            'reports' => $reports,
            'keyword' => $keyword
        ];
    	return view('admin/admin/report/list_search')->with($data);
    }

    public function resolve($id, $report_id)
    {
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $reports = Report::reportsResolve($token, $report_id);
        $data=[
            'title' => 'Reports',
            'user_id'=> $id,
            'access_level' => $access_level,
            'reports' => $reports
        ];
    	return redirect("/admin/$id/reports/1");
    }

    public function export()
    {
        return Excel::download(new ReportsExport(), 'Reports.xlsx');
    }
}
