<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
// use Facades\App\Helper\Keytapoint;
use Facades\App\Helper\Gamification;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class GamificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role_access:user');
        $this->middleware('role_access:admin');
    }

    public function indexReward($id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $rewards = Gamification::indexReward($token);
        $data=[
            'title' => 'Daily Reward',
            'user_id'=> $id,
            'access_level' => $access_level,
            'rewards' => $rewards
        ];

    	return view('admin/admin/gamification/reward/list')->with($data);
    }

    public function detailReward($id, $day)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $rewards = Gamification::detailReward($token, $id, $day);
        $data=[
            'title' => 'Daily Reward',
            'user_id'=> $id,
            'access_level' => $access_level,
            'rewards' => $rewards
        ];
        
    	return view('admin/admin/gamification/reward/edit')->with($data);
    }

    public function updateReward(Request $request, $id, $reward_id)
    {
        $imageUrl = $request->file('imageUrl');
        $day = $request->post("day");
        $value = $request->post("value");
        $title = $request->post("title");
        $description = $request->post("description");

        $data = [
            [
                'name' => 'day',
                'contents' => $day,
            ],
            [
                'name' => 'value',
                'contents' => $value,
            ],
            [
                'name' => 'title',
                'contents' => $title,
            ],
            [
                'name' => 'description',
                'contents' => $description,
            ]
        ];

        if ($imageUrl != null) {
            array_push($data,  [
                'name'     => 'imageUrl',
                'contents' => file_get_contents($imageUrl),
                'filename' => $imageUrl->getClientOriginalName()
            ]);
        }
        
        $token = Helper::getToken();
        $rewards = Gamification::updateReward($token, $reward_id, $data);

    	return back()->with(['message' => 'Ubah Reward Berhasil']);
    }

    public function indexWheel($id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $wheels = Gamification::indexWheel($token);
        $data=[
            'title' => 'Wheel of Fortune',
            'user_id'=> $id,
            'access_level' => $access_level,
            'wheels' => $wheels
        ];

    	return view('admin/admin/gamification/wheel/list')->with($data);
    }

    public function detailWheel($id, $wheel_id)
	{
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $wheels = Gamification::detailWheel($token, $wheel_id);
        $data=[
            'title' => 'Wheel of Fortune',
            'user_id'=> $id,
            'access_level' => $access_level,
            'wheels' => $wheels
        ];

    	return view('admin/admin/gamification/wheel/edit')->with($data);
    }

    public function updateWheel(Request $request, $id, $wheel_id)
    {
        $voucher_type = $request->post('voucher_type');
        $code = $request->post('code');
        $expedition_ids = $request->post('expedition_ids');
        $voucher_title = $request->post('voucher_title');
        $description = $request->post('description');
        $value = $request->post('value');
        $unit = $request->post('unit');
        $max_value = $request->post('max_value');
        $quantity = $request->post('quantity');
        $probabilities = $request->post('probabilities');
        $color = $request->post('color');
        $promo_template = $request->post('promo_template');
        $detail_promo = $request->post('detail_promo');
        $voucher_image = $request->file('voucher_image');
        $voucher_promo_image = $request->file('voucher_promo_image');
        $wheel_image = $request->file('wheel_image');

        $data = [
            [
                'name' => 'voucher_type',
                'contents' => $voucher_type,
            ],
            [
                'name' => 'code',
                'contents' => $code,
            ],
            [
                'name' => 'expedition_ids',
                'contents' => $expedition_ids,
            ],
            [
                'name' => 'voucher_title',
                'contents' => $voucher_title,
            ],
            [
                'name' => 'description',
                'contents' => $description,
            ],
            [
                'name' => 'value',
                'contents' => $value,
            ],
            [
                'name' => 'unit',
                'contents' => $unit,
            ],
            [
                'name' => 'max_value',
                'contents' => $max_value,
            ],
            [
                'name' => 'quantity',
                'contents' => $quantity,
            ],
            [
                'name' => 'probabilities',
                'contents' => $probabilities,
            ],
            [
                'name' => 'color',
                'contents' => $color,
            ],
            [
                'name' => 'promo_template',
                'contents' => $promo_template,
            ],
            [
                'name' => 'detail_promo',
                'contents' => $detail_promo,
            ]
        ];

        if ($voucher_image != null) {
            array_push($data,  [
                'name'     => 'voucher_image',
                'contents' => file_get_contents($voucher_image),
                'filename' => $voucher_image->getClientOriginalName()
            ]);
        }
        if ($voucher_promo_image != null) {
            array_push($data,  [
                'name'     => 'voucher_promo_image',
                'contents' => file_get_contents($voucher_promo_image),
                'filename' => $voucher_promo_image->getClientOriginalName()
            ]);
        }
        if ($wheel_image != null) {
            array_push($data, [
                'name'     => 'wheel_image',
                'contents' => file_get_contents($wheel_image),
                'filename' => $wheel_image->getClientOriginalName()
            ]);
        }
        
        $token = Helper::getToken();
        $rewards = Gamification::updateWheel($token, $wheel_id, $data);

    	return back()->with(['message' => 'Ubah Wheel Berhasil']);
    }
    
    public function indexHistory(Request $request, $id, $page)
	{
        $day = $request->get('day');
        $voucherReward = $request->get('voucherReward');
        $userName = $request->get('userName');
        $date_range= $request->get('range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $points = Gamification::listHistory($token, $page);
        $data=[
            'title' => 'Daily Reward History',
            'user_id'=> $id,
            'access_level' => $access_level,
            'points' => $points,
            'day' => $day,
            'voucherReward' => $voucherReward,
            'userName' => $userName,
            'date_range' => $date_range
        ];
    
    	return view('admin/admin/gamification/history/list')->with($data);
    }

    public function searchHistory(Request $request, $id, $page)
	{
        $day = $request->get('day');
        $voucherReward = $request->get('voucherReward');
        $userName = $request->get('userName');
        $date_range= $request->get('range');
        if (!$date_range){
            $start_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " - 365 day"));
            $end_date = date("Y-m-d");
            $date_range = "{$start_date}-{$end_date}";
        }
        $body = [
            'page' => $page, 
            'per_page' => 25,
            'day' => $day,
            'voucherReward' => $voucherReward,
            'userName' => $userName,
            'range' => $date_range
        ];
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $points = Gamification::searchHistory($token, $page, $body);
        
        $data=[
            'title' => 'Daily Reward History',
            'user_id'=> $id,
            'access_level' => $access_level,
            'points' => $points,
            'day' => $day,
            'voucherReward' => $voucherReward,
            'userName' => $userName,
            'date_range' => $date_range
        ];
    
    	return view('admin/admin/gamification/history/list_search')->with($data);
    }
}
