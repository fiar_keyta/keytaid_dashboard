<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Canvaser;
use Facades\App\Helper\Keytauser;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class ReferralController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id, $page)
	{
        $community_name = $request->get('community_name');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $users = Keytauser::referralList($token, $page);
        $data=[
            'title' => 'Referral',
            'user_id'=> $id,
            'access_level' => $access_level,
            'users' => $users,
        ];

    	return view('admin/admin/referral/list')->with($data);
    }
    
}
