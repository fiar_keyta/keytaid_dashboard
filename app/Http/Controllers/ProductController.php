<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Product;
use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page) 
	{
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $products = Product::productsList($token, $page);
        $data=[
            'title' => 'Products',
            'user_id'=> $id,
            'access_level' => $access_level,
            'products' => $products
        ];
    	return view('admin/admin/product/list')->with($data);
    }
    
    public function search(Request $request, $id, $page)
    {
        $keyword = $request->get('keyword');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $products = Product::productsSearch($token, $keyword, $page);
        $data=[
            'title' => 'Products',
            'user_id'=> $id,
            'access_level' => $access_level,
            'products' => $products,
            'keyword' => $keyword
        ];
    	return view('admin/admin/product/list_search')->with($data);
    }

    public function export()
    {
        return Excel::download(new productsExport(), 'Products.xlsx');
    }
}
