<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\Canvaser;
use Facades\App\Helper\Keytauser;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class KeytauserCommunityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role_access:user');
        // $this->middleware('role_access:admin');
    }

    public function details(Request $request, $id, $shop_id, $page)
	{
        
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $users = Keytauser::userCommunityInv($token, $shop_id, $page, $access_level["referral_code"]);

        $data=[
            'title' => 'Detail transaksi',
            'user_id'=> $id,
            'access_level' => $access_level,
            'users' => $users,
        ];
    	return view('admin/admin/community_canvas/list_invoice')->with($data);
    }
    
}
