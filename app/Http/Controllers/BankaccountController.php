<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Facades\App\Helper\Helper;
use Facades\App\Helper\BankAccount;
use App\Exports\BanksExport;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;

class BankaccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_access:admin');
    }

    public function index(Request $request,$id, $page)
	{
        $filter = $request->get('filter');
        $keyword = $request->get('keyword');
        $token = Helper::getToken();
        $access_level = \App\User::find($id);
        $banks = BankAccount::bankAccountList($token, $page, $filter);
        $data=[
            'title' => 'Bank Account',
            'user_id'=> $id,
            'access_level' => $access_level,
            'banks' => $banks,
            'filter' => $filter
        ];
    	return view('admin/admin/bank_account/list')->with($data);
    }
    
    public function search(Request $request, $id, $page)
    {
        $keyword = $request->get('keyword');
        $filter = $request->get('filter');
        $access_level = \App\User::find($id);
        $token = Helper::getToken();
        $banks = BankAccount::bankAccountSearch($token, $keyword, $page);
        $data=[
            'title' => 'Bank Account',
            'user_id'=> $id,
            'access_level' => $access_level,
            'banks' => $banks,
            'keyword' => $keyword,
            'filter' => $filter
        ];
    	return view('admin/admin/bank_account/list_search')->with($data);
    }

    public function export()
    {
        return Excel::download(new BanksExport(), 'Banks.xlsx');
    }
}
