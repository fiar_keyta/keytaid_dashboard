<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AccesLevelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $akses)
    {
        if (auth()->check() && !auth()->user()->roleCheck($akses)) {
            if ($akses == 'admin') {
                return route('admin');
            }
            elseif ($akses == 'user') {
                return route('canvaser');
            }
            elseif($akses == 'superuser'){
                return route('superuser');
            }
        }
        return $next($request);
    }
}
