@extends('admin/admin')
@section('content')

<!-- Main content -->
<div class="row">


  <div class="col-md-12">
    <form method="POST" action="{{ URL::to('superuser/tambahadmin/'.$user_id.'/post') }}" >
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="card">
        <div class="card-body">
          
          @if(!empty($errors->all()))
          <div class="alert alert-danger">
            {{ Html::ul($errors->all())}}
          </div>
          @endif


          <div class="text-center">
            <h2>Tambah Admin</h2>
          </div>
          <br>
          <div class="form-group has-feedback">
            {{ Form::label('username', 'Username') }}
            <input type="text" class="form-control" placeholder="Username" name="username">
          </div>
          <div class="form-group has-feedback">
            {{ Form::label('password', 'Password') }}
            <input type="password" class="form-control" placeholder="Password" name="password">
          </div>
          <div class="form-group has-feedback">
            {{ Form::label('password', 'Password Retype') }}
            <input type="password" class="form-control" placeholder="Retype Password" name="password_confirmation">
          </div>
          <div class="form-group has-feedback">
            {{ Form::label('nama_lengkap', 'Nama Lengkap') }}
            <input type="text" class="form-control" placeholder="Nama Lengkap" name="name">
          </div>
          <div class="form-group has-feedback">
            {{ Form::label('email', 'Email') }}
            <input type="text" class="form-control" placeholder="Email" name="email" >
          </div>

        </div><!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary pull-right">Tambah Admin</button>
        </div>
      </div>
    </form>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- /.content -->

@endsection