@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                        <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('/admin/'.$user_id.'/notification/'.$notifications['results']['id'].'/update') }}" enctype="multipart/form-data"> 
                        @method('PUT')
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('topic', 'Topic') }}
                                                <input type="text" class="form-control" placeholder="topic" name="topic" value="{{ $notifications['results']['topic']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('title', 'Title') }}
                                                <input type="text" class="form-control" placeholder="title" name="title" value="{{ $notifications['results']['title']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('event_name', 'Event Name') }}
                                                <input type="text" class="form-control" placeholder="event_name" name="event_name" value="{{ $notifications['results']['event_name']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('description', 'Description') }}
                                                <input type="text" class="form-control" placeholder="description" name="description" value="{{ $notifications['results']['description']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('show_time', 'Show Time') }}
                                                <input type="text" class="form-control" placeholder="show_time" name="show_time" value="{{ $notifications['results']['show_time']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('show_day', 'Show Day') }}
                                                <input type="text" class="form-control" placeholder="show_day" name="show_day" value="{{ $notifications['results']['show_day']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('is_enable', 'Is Enabled') }}
                                                <input type="text" class="form-control" placeholder="is_enable" name="is_enable" value="{{ $notifications['results']['is_enable']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('start_date', 'Start Date') }}
                                                <input type="text" class="form-control" placeholder="start_date" name="start_date" value="{{ $notifications['results']['start_date']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('end_date', 'End Date') }}
                                                <input type="text" class="form-control" placeholder="end_date" name="end_date" value="{{ $notifications['results']['end_date']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('notification_type', 'Notification Type') }}
                                                <input type="text" class="form-control" placeholder="notification_type" name="notification_type" value="{{ $notifications['results']['notification_type']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('image_url', 'Image URL') }}
                                                <input type="text" class="form-control" placeholder="image_url" name="image_url" value="{{ $notifications['results']['image_url']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('redirect_url', 'Redirect URL') }}
                                                <input type="text" class="form-control" placeholder="redirect_url" name="redirect_url" value="{{ $notifications['results']['redirect_url']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary button-prevent-multiple-submits">
                                                Update Notification Detail
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })
    })
</script>
@endsection
