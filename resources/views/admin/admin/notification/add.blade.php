@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                        <form class="form-prevent-multiple-submits" method="POST" action="{{ URL::to('/admin/'.$user_id.'/notification/create/new') }}" enctype="multipart/form-data"> 
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('topic', 'Topic') }}
                                                <input type="text" class="form-control" placeholder="topic" name="topic">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('title', 'Title') }}
                                                <input type="text" class="form-control" placeholder="title" name="title">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('event_name', 'Event Name') }}
                                                <input type="text" class="form-control" placeholder="event_name" name="event_name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('description', 'Description') }}
                                                <input type="text" class="form-control" placeholder="description" name="description">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('show_time', 'Show Time') }}
                                                <input type="text" class="form-control" placeholder="21:00:00" name="show_time">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('show_day', 'Show Day') }}
                                                <input type="text" class="form-control" placeholder="0,1,2,3,4,5,6" name="show_day">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('is_enable', 'Is Enabled') }}
                                                <input type="text" class="form-control" placeholder="1 / 0" name="is_enable">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('start_date', 'Start Date') }}
                                                <input type="text" class="form-control" placeholder="YYYY-MM-DD" name="start_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('end_date', 'End Date') }}
                                                <input type="text" class="form-control" placeholder="YYYY-MM-DD" name="end_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('notification_type', 'Notification Type') }}
                                                <input type="text" class="form-control" placeholder="data / notification" name="notification_type">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="form-group">
                                                {{ Form::label('image_url', 'Upload Image') }}
                                                {{ Form::file('image_url', ['class'=>'form-control']) }}  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('redirect_url', 'Redirect URL') }}
                                                <input type="text" class="form-control" placeholder="redirect_url" name="redirect_url">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-warning button-prevent-multiple-submits">
                                                Add New Notification
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.form-prevent-multiple-submits').on('submit', function() {
            $('.button-prevent-multiple-submits').attr('disabled', 'true');
        })
    })
</script>
@endsection
