@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                    <div class="card-tools">
                        <a class="btn btn-primary" href="{{ URL::to('/admin/'.$user_id.'/notification/create') }}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif
                    
                    @if($access_level['role_id']==2)

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-bell"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Notification</span>
                                    <span class="info-box-number">{{ $notifications['total'] }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endif
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Topic</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Time</th>
                                    <th>Day</th>
                                    <th>Event Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Is Enabled</th>
                                    <th>Type</th>
                                    <th>Image</th>
                                    <th>Redirect URL</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1;
                                ?>
                                @foreach($notifications['results'] as $notification)
                                    <tr>
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $notification['topic'] }}</td>
                                        <td class="text-center">{{ $notification['title'] }}</td>
                                        <td class="text-center">{{ $notification['description'] }}</td>
                                        <td class="text-center">{{ $notification['status'] }}</td>
                                        <td class="text-center">{{ $notification['show_time'] }}</td>
                                        <td class="text-center">{{ $notification['show_day'] }}</td>
                                        <td class="text-center">{{ $notification['event_name'] }}</td>
                                        <td class="text-center">{{ $notification['start_date'] }}</td>
                                        <td class="text-center">{{ $notification['end_date'] }}</td>
                                        <td class="text-center">{{ $notification['is_enable'] }}</td>
                                        <td class="text-center">{{ $notification['notification_type'] }}</td>
                                        <td class="text-center">{{ $notification['image_url'] }}</td>
                                        <td class="text-center">{{ $notification['redirect_url'] }}</td>
                                        <td class="text-center">
                                            <a href="{{ URL::to('/admin/'.$user_id.'/notification/detail/'.$notification['id']) }}">
                                                <i class="btn btn-primary fa fa-pencil nav-icon"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection