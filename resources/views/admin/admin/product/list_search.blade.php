@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                    
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/products/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/products/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                </div>
                            </form>
                        </div>
                    </div> 

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Shop Id</th>
                                <th>Nama Toko</th>
                                <th>Nama Produk</th>
                                <th>Harga Produk</th>
                                <th>Diskon</th>
                                <th>Length</th>
                                <th>Width</th>
                                <th>Height</th>
                                <th>Weight</th>
                                <!-- <th>Action</th>  -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1 + (($products['current_page']-1)*$products['limit']);
                                // echo $products
                            ?>

                            @foreach($products['result'] as $product)
                            <tr>
                            
                                    <td class="text-center">{{ $no++ }}</td>
                                    @if(isset($product['shop']))
                                        <td class="text-center">{{ $product['shop']['id'] }}</td>
                                        <td class="text-center">{{ $product['shop']['name'] }}</td>
                                    @else
                                        <td class="text-center"></td>
                                        <td class="text-center"></td>
                                    @endif
                                    <td class="text-center">{{ $product['name'] }}</td>  
                                    <td class="text-center">{{ $product['price'] }}</td>
                                    <td class="text-center">{{ $product['discount'] }}</td>
                                    <td class="text-center">{{ $product['length'] }}</td>
                                    <td class="text-center">{{ $product['width'] }}</td>
                                    <td class="text-center">{{ $product['height'] }}</td>
                                    <td class="text-center">{{ $product['weight'] }}</td>
                                    
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $products['current_page'] + 2; 
                                    $start_page = $products['current_page'] - 2;
                                ?>
                                
                                @if ($products['current_page'] > 1)
                                    <?php $before_page = $products['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/products/search/1?keyword='.$keyword) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/products/search/'.$before_page.'?keyword='.$keyword) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $products['total_pages'])
                                        <li class="page-item @if($products['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/products/search/'.$i.'?keyword='.$keyword) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($products['current_page'] < $products['total_pages'])
                                    <?php $next_page = $products['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/products/search/'.$next_page.'?keyword='.$keyword) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/products/search/'.$products['total_pages'].'?keyword='.$keyword) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection