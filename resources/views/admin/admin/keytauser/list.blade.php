@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif

                    <div class="row">
                        @if($access_level['role_id']==2)
                            <form class="col-6" id="form_tanggal" method="GET"
                                action="{{ URL::to('/admin/'.$user_id.'/keytauser/1?community_name='.$community_name.'&sort_by='.$sort_by.'&sort_table_name='.$sort_table_name.'&sort_column_name='.$sort_column_name.'&date_range='.$date_range) }}">
                                <div class="form-group">
                                    <label>Pilih Tanggal:</label>

                                    <div class="input-group">
                                        <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                            <i class="fa fa-calendar"></i> {{$date_range}}
                                            <i class="fa fa-caret-down"></i>
                                            <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                            <input type="hidden" name="community_name" id="community_name" value="{{$community_name}}" />
                                            <input type="hidden" name="sort_by" id="sort_by" value="{{$sort_by}}" />
                                            <input type="hidden" name="sort_table_name" id="sort_table_name" value="{{$sort_table_name}}" />
                                            <input type="hidden" name="sort_column_name" id="sort_column_name" value="{{$sort_column_name}}" />
                                        </button>

                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                    
                    @if($access_level['role_id']==2)

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Registration Attempts</span>
                                    <span class="info-box-number">{{$dashboard['total_user']}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-shopping-cart"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total User With Shop</span>
                                    <span class="info-box-number">{{$users['total_registered']}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Last 7 Days User With Shop</span>
                                    <span class="info-box-number">{{$users['weekly']}}</span>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Monthly User Open Apps</span>
                                    <span class="info-box-number">{{$users['monthly_open']}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Weekly User Open Apps</span>
                                    <span class="info-box-number">{{$users['weekly_open']}}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Daily User Open Apps</span>
                                    <span class="info-box-number">{{$users['daily_open']}}</span>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-3 text-center">
                                <span></span>
                            </div>
                            <div class="col-1 text-center">
                                <a href="{{URL::to('/admin/'.$user_id.'/keytauser/export')}}">
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </a>
                            </div>
                            <div class="col-5">
                                <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/keytauser/search/1') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="">
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($community_name == null)
                                        Select Community
                                    @else
                                        {{$community_name}}
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach ($community_lists['communities'] as $community)
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/keytauser/1?community_name='.$community['name'].'&date_range='.$date_range.'&sort_by='.$sort_by.'&sort_table_name=users&sort_column_name=id') }}">{{$community['name']}}</a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($sort_table_name == null)
                                        Select Sort by
                                    @else
                                        {{$sort_table_name." ".$sort_column_name}}
                                    @endif
                                </button>
                                
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/keytauser/1?community_name='.$community_name.'&date_range='.$date_range.'&sort_by='.$sort_by.'&sort_table_name=users&sort_column_name=id') }}">User ID</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/keytauser/1?community_name='.$community_name.'&date_range='.$date_range.'&sort_by='.$sort_by.'&sort_table_name=shops&sort_column_name=id') }}">Shop ID</a>
                                </div>
                            </div>
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($sort_by == null)
                                        Select Sort type
                                    @else
                                        {{$sort_by}}
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/keytauser/1?community_name='.$community_name.'&sort_by=DESC&sort_table_name='.$sort_table_name.'&sort_column_name='.$sort_column_name) }}">Descending</a>
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/keytauser/1?community_name='.$community_name.'&sort_by=ASC&sort_table_name='.$sort_table_name.'&sort_column_name='.$sort_column_name) }}">Ascending</a>
                                </div>
                            </div>
                        </div>
                        <br>

                    @elseif($access_level['role_id']==1)
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Registered</span>
                                    <span class="info-box-number">{{$users['total']}}</span>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <form class="col-6" id="form_tanggal" method="GET"
                                action="{{ URL::to('/canvaser/'.$user_id.'/keytauser/1?date_range='.$date_range) }}">
                                <div class="form-group">
                                    <label>Pilih Tanggal:</label>

                                    <div class="input-group">
                                        <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                            <i class="fa fa-calendar"></i> {{$date_range}}
                                            <i class="fa fa-caret-down"></i>
                                            <input type="hidden" name="referrer" value="{{$referrer}}">
                                            <input type="hidden" name="keyword" value="{{$keyword}}">
                                            <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="row">
                            <div class="col-1">
                                <form class="form-prevent-multiple-submits" method="GET" action="{{URL::to('/canvaser/'.$user_id.'/keytauser/export')}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <button type="submit" class="btn btn-primary button-prevent-multiple-submits">Export</button>
                                </form>
                            </div>
                            <div class="col-5">
                                <form method="GET" action="{{ URL::to('/canvaser/'.$user_id.'/keytauser/1') }}" enctype="multipart/form-data">
                                    <!-- <input type="hidden" name="_token" value="{{csrf_token()}}"> -->
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{null}}">
                                        <input type="hidden" name="referrer" value="{{$referrer}}">
                                        <input type="hidden" name="date_range" value="{{$date_range}}">
                                    </div>
                                </form>
                            </div>
                            <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(str_contains($referrer, ','))
                                        Select Sort type
                                    @else
                                        @if($referrer != "LUNASUMKM")
                                            LUNAS WARUNG
                                        @else
                                            LUNAS UMKM
                                        @endif
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ URL::to('/canvaser/'.$user_id.'/keytauser/1?referrer=LUNASUMKM&keyword='.$keyword.'&date_range='.$date_range) }}">LUNAS UMKM</a>
                                    <a class="dropdown-item" href="{{ URL::to('/canvaser/'.$user_id.'/keytauser/1?referrer=4301DZ0D0JQ7&keyword='.$keyword.'&date_range='.$date_range) }}">LUNAS WARUNG</a>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endif
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    @if($access_level['role_id']==2)
                                        <th>User ID</th>
                                        <th>Shop Id</th>
                                        <th>Tanggal Registrasi</th>
                                        <th>Nomor HP</th>
                                        <th>Nama</th>
                                        <th>Nama Toko</th>
                                        <th>Alamat Toko</th>
                                        <th>Referrer</th>
                                        <th>Community</th>
                                    @elseif($access_level['role_id']==1)
                                        <th>Tanggal Registrasi</th>
                                        <th>Nomor HP</th>
                                        <th>Nama</th>
                                        <th>Nama Toko</th>
                                        <th>Alamat Toko</th>
                                        <th>Referrer</th>
                                        <th>Jumlah invoice</th>
                                        <th>Rincian Invoice</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($users['current_page']-1)*$users['limit']);
                                    // echo $users
                                ?>

                                @foreach($users['results'] as $user)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    @if($access_level['role_id']==2)
                                        <td class="text-center">{{ $user['id'] }}</td>
                                        <td class="text-center">{{ $user['shop_id'] }}</td>
                                        <td class="text-center">{{ $user['created_at'] }}</td>
                                        <td class="text-center">{{ $user['phone_with_code'] }}</td>
                                        <td class="text-center">{{ $user['name'] }}</td>
                                        @if(isset($user['shop']))
                                            <td class="text-center">{{ $user['shop']['name'] }}</td>
                                            <td class="text-center">{{ $user['shop']['address'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                        @endif
                                        @if(isset($user['referrer']))
                                            <td class="text-center">{{ $user['referrer'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                        @endif
                                        @if(isset( $user['shop']['community']))
                                            <td class="text-center">{{ $user['shop']['community']['name'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                        @endif
                                    @elseif($access_level['role_id']==1)
                                        <td class="text-center">{{ str_replace("T"," ",substr($user['created_at'], 0, 19)) }}</td>
                                        <td class="text-center">{{ $user['phone_with_code'] }}</td>
                                        <td class="text-center">{{ $user['name'] }}</td>
                                        @if(isset($user['shop']))
                                            <td class="text-center">{{ $user['shop']['name'] }}</td>
                                            <td class="text-center">{{ $user['shop']['address'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                        @endif
                                        @if(isset($user['referrer']))
                                            <td class="text-center">{{ $user['referrer'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                        @endif
                                        @if(isset($user['trx_count']))
                                            <td class="text-center">{{ $user['trx_count'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                        @endif
                                        <td class="text-center">
                                            <!-- <div class="col-5"> -->
                                                <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/keytauser/community/shop/'.$user['shop_id'].'/1') }}" enctype="multipart/form-data">
                                                    <button type="submit" class="btn btn-info">details</button>
                                                </form>
                                            <!-- </div> -->
                                        </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    @if($access_level['role_id'] == 2)
                        <div class="mt-2 d-flex justify-content-center">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <?php 
                                        $show_page = $users['current_page'] + 2; 
                                        $start_page = $users['current_page'] - 2;
                                    ?>
                                    
                                    @if ($users['current_page'] > 1)
                                        <?php $before_page = $users['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/1?community_name='.$community_name.'&date_range='.$date_range.'&sort_by='.$sort_by.'&sort_table_name='.$sort_table_name.'&sort_column_name='.$sort_column_name) }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/'.$before_page.'?community_name='.$community_name.'&date_range='.$date_range.'&sort_by='.$sort_by.'&sort_table_name='.$sort_table_name.'&sort_column_name='.$sort_column_name) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $users['total_pages'])
                                            <li class="page-item @if($users['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/'.$i.'?community_name='.$community_name.'&date_range='.$date_range.'&sort_by='.$sort_by.'&sort_table_name='.$sort_table_name.'&sort_column_name='.$sort_column_name) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($users['current_page'] < $users['total_pages'])
                                        <?php $next_page = $users['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/'.$next_page.'?community_name='.$community_name.'&date_range='.$date_range.'&sort_by='.$sort_by.'&sort_table_name='.$sort_table_name.'&sort_column_name='.$sort_column_name) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/'.$users['total_pages'].'?community_name='.$community_name.'&date_range='.$date_range.'&sort_by='.$sort_by.'&sort_table_name='.$sort_table_name.'&sort_column_name='.$sort_column_name) }}">Last</a></li>
                                    @endif
                                    
                                </ul>
                            </nav>
                        </div>
                    @elseif($access_level['role_id'] == 1)
                        <div class="mt-2 d-flex justify-content-center">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <?php 
                                        $show_page = $users['current_page'] + 2; 
                                        $start_page = $users['current_page'] - 2;
                                    ?>
                                    
                                    @if ($users['current_page'] > 1)
                                        <?php $before_page = $users['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/keytauser/1?referrer='.$referrer.'&keyword='.$keyword.'&date_range='.$date_range) }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/keytauser/'.$before_page.'?referrer='.$referrer.'&keyword='.$keyword.'&date_range='.$date_range) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $users['total_pages'])
                                            <li class="page-item @if($users['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/keytauser/'.$i.'?referrer='.$referrer.'&keyword='.$keyword.'&date_range='.$date_range) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($users['current_page'] < $users['total_pages'])
                                        <?php $next_page = $users['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/keytauser/'.$next_page.'?referrer='.$referrer.'&keyword='.$keyword.'&date_range='.$date_range) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/canvaser/'.$user_id.'/keytauser/'.$users['total_pages'].'?referrer='.$referrer.'&keyword='.$keyword.'&date_range='.$date_range) }}">Last</a></li>
                                    @endif
                                    
                                </ul>
                            </nav>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>

    <script>
        $(function(){
            $('.form-prevent-multiple-submits').on('submit', function() {
                $('.button-prevent-multiple-submits').attr('disabled', 'true');
            })
        })
    </script>
@endsection