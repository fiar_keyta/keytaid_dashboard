@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/costs/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div> 
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <form id="form_tanggal" method="GET" action="{{ URL::to('/admin/'.$user_id.'/costs/1') }}">
                        <div class="form-group">
                            <label>Pilih Tanggal:</label>
                    
                            <div class="input-group">
                                <button type="button" class="btn btn-default float-right" id="daterange-btn" >
                                <i class="fa fa-calendar"></i> {{$date_range}}
                                <i class="fa fa-caret-down"></i>
                                <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                </button>
                                
                            </div>
                        </div>
                    </form>

                    <h4>{{$date_range}}</h4>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>
                            
                            <div class="info-box-content">
                                <span class="info-box-text">Total Cek Ongkir</span>
                                <span class="info-box-number">{{$costs['total']}}</span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">User Cek Ongkir</span>
                                <span class="info-box-number">{{$costs['total_users']}}</span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Last 24 Hour Cek Ongkir</span>
                                <span class="info-box-number">{{$costs["daily"]}}</span>
                            </div>
                            </div>
                        </div>
                    </div>

                    <h4>Partner</h4>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Biteship Usage</span>
                                <span class="info-box-number">{{$costs['biteship']}}</span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Grab Usage</span>
                                <span class="info-box-number">{{$costs['grab']}}</span>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Anteraja Usage</span>
                                <span class="info-box-number">{{$costs["anteraja"]}}</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-3 text-center">
                        <span></span>
                    </div>
                    <br>

                    <div class="col-6">
                        <form method="GET"
                            action="{{ URL::to('/admin/'.$user_id.'/costs/search/1') }}"
                            enctype="multipart/form-data">
                            
                            <div class="md-form active-cyan active-cyan-2 mb-3">
                                <input type="hidden" name="date_range" value="{{$date_range}}">
                                <input class="form-control" type="text" placeholder="Search by shop id"
                                    aria-label="Search" name="keyword" value="">
                            </div>
                        </form>
                    </div>
                    </div>
                    <br>

                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Shop Id</th>
                                    <th>Nama Toko</th>
                                    <th>Origin Address</th>
                                    <th>Origin Latitude</th>
                                    <th>Origin Longitude</th>
                                    <th>Origin Postcode</th>
                                    <th>Destination Address</th>
                                    <th>Destination Latitude</th>
                                    <th>Destination Longitude</th>
                                    <th>Destination Postcode</th>
                                    <th>Nama Ekspedition</th>
                                    <th>Partner</th>
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($costs['current_page']-1)*$costs['limit']);
                                    // echo $costs
                                ?>

                                @foreach($costs['results'] as $cost)
                                <tr>
                                
                                        <td class="text-center">{{ $no++ }}</td>
                                        @if(isset($cost['shop']))
                                            <td class="text-center">{{ $cost['shop']['id'] }}</td>
                                            <td class="text-center">{{ $cost['shop']['name'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                        @endif
                                        <td class="text-center">{{ $cost['origin_detail'] }}</td>
                                        <td class="text-center">{{ $cost['origin_latitude'] }}</td>
                                        <td class="text-center">{{ $cost['origin_longitude'] }}</td>
                                        <td class="text-center">{{ $cost['origin_postcode'] }}</td>
                                        <td class="text-center">{{ $cost['dest_detail'] }}</td>
                                        <td class="text-center">{{ $cost['dest_latitude'] }}</td>
                                        <td class="text-center">{{ $cost['dest_longitude'] }}</td>
                                        <td class="text-center">{{ $cost['dest_postcode'] }}</td>
                                        <td class="text-center">{{ $cost['courier'] }}</td>
                                        <td class="text-center">{{ $cost['partner'] }}</td>
                                        
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $costs['current_page'] + 2; 
                                    $start_page = $costs['current_page'] - 2;
                                ?>
                                
                                @if ($costs['current_page'] > 1)
                                    <?php $before_page = $costs['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/costs/1?filter='.$filter) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/costs/'.$before_page.'?filter='.$filter) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $costs['total_pages'])
                                        <li class="page-item @if($costs['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/costs/'.$i.'?filter='.$filter) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($costs['current_page'] < $costs['total_pages'])
                                    <?php $next_page = $costs['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/costs/'.$next_page.'?filter='.$filter) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/costs/'.$costs['total_pages'].'?filter='.$filter) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection