@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                        <form method="POST" action="{{ URL::to('admin/'.$user_id.'/gamification/wheel/'.$wheels['data']['id'].'/edit') }}" enctype="multipart/form-data"> 
                        @method('PATCH')
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <img src="{{$wheels['data']['voucher_image']}}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('voucher_type', 'Voucher Type') }}
                                            <div class="form-group"> 
                                                <select name="voucher_type" class="form-control">
                                                    <option>cashback</option>
                                                    <option>discount</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('code', 'Code') }}
                                                <input type="text" class="form-control" placeholder="Code" name="code" value="{{$wheels['data']['code']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group has-feedback">
                                                {{ Form::label('Expedition id', 'Expedition Ids') }}
                                                <input type="text" class="form-control" placeholder="Expedition Ids" name="expedition_ids" value="{{$wheels['data']['expedition_ids']}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('voucher_title', 'Voucher Title') }}
                                            <input type="text" class="form-control" placeholder="Voucher Title" name="voucher_title" value="{{$wheels['data']['voucher_title']}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('description', 'Voucher Description') }}
                                            <input type="text" class="form-control" placeholder="Description" name="description" value="{{$wheels['data']['description']}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('value', 'Voucher Value') }}
                                            <input type="text" class="form-control" placeholder="Voucher Value" name="value" value="{{$wheels['data']['value']}}">
                                        </div>
                                    </div>
                                    <div class=col-md-2>
                                        <div class="form-group has-feedback">
                                            {{ Form::label('unit', 'Unit') }}
                                            <div class="form-group"> 
                                                <select name="unit" class="form-control">
                                                    <option>percent</option>
                                                    <option>price</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('max_value', 'Maximum Value') }}
                                            <input type="text" class="form-control" placeholder="Max Value" name="max_value" value="{{$wheels['data']['max_value']}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('quantity', 'Quantity') }}
                                            <input type="text" class="form-control" placeholder="Quantity" name="quantity" value="{{$wheels['data']['quantity']}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('probabilities', 'Probabilities') }}
                                            <input type="text" class="form-control" placeholder="Probabilities" name="probabilities" value="{{$wheels['data']['probabilities']}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('color', 'Color') }}
                                            <input type="text" class="form-control" placeholder="Color" name="color" value="{{$wheels['data']['color']}}">
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('promo_template', 'Promo Template') }}
                                            <input type="text" class="form-control" placeholder="Template #promo_price, #promo_percent, #logo" name="promo_template" value="{{$wheels['data']['promo_template']}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group has-feedback">
                                            {{ Form::label('detail_promo', 'Voucher Detail') }}
                                            <textarea class="col-md-12" name="detail_promo" id="" rows="6"  placeholder="Detail promo" >{{$wheels['data']['detail_promo']}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-4">
                                        <img src="{{$wheels['data']['voucher_image']}}" width="100px">
                                    </div>
                                    <div class="col-md-4">
                                        <img src="{{$wheels['data']['voucher_promo_image']}}" width="100px">
                                    </div>
                                    <div class="col-md-4">
                                        <img src="{{$wheels['data']['wheel_image']}}" width="100px">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group">
                                                {{ Form::label('voucher_image', 'Upload Image') }}
                                                {{ Form::file('voucher_image', ['class'=>'form-control']) }}  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group">
                                                {{ Form::label('voucher_promo_image', 'Upload Promo') }}
                                                {{ Form::file('voucher_promo_image', ['class'=>'form-control']) }}  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <div class="form-group">
                                                {{ Form::label('wheel_image', 'Upload wheel') }}
                                                {{ Form::file('wheel_image', ['class'=>'form-control']) }}  
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary">
                                                Update wheel voucher
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
