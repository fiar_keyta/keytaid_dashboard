@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif
                    
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Voucher Image</th>
                                    <th>Code</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Value</th>
                                    <th>Unit</th>
                                    <th>Max Value</th>
                                    <th>Probabilities</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <?php 
                                    $no = 1
                                ?> -->

                                @foreach($wheels['data'] as $wheel)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center"><img src="{{ $wheel['voucher_image'] }}" width="100px"></td>
                                    <td class="text-center">{{ $wheel['code'] }}</td>
                                    <td class="text-center">{{ $wheel['voucher_title'] }}</td>
                                    <td class="text-center">{{ $wheel['description'] }}</td>
                                    <td class="text-center">{{ $wheel['value'] }}</td>
                                    <td class="text-center">{{ $wheel['unit'] }}</td>
                                    <td class="text-center">Rp. {{ $wheel['max_value'] }}</td>
                                    <td class="text-center">{{ $wheel['probabilities'] }}%</td>
                                    <td class="text-center"><a href="{{ URL::to('/admin/'.$user_id.'/gamification/wheel/'.$wheel['id']) }}" class="btn btn-success">Edit</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = 1;
                                    $start_page = 1;
                                ?>
                                
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection