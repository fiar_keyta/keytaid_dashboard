@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        <a class="btn btn-primary" href="{{ URL::to('/admin/'.$user_id.'/app-management/faq/add') }}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif
                    {{-- <div class="scrolling-wrapper"> --}}
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Category</th>
                                    <th>Category Order</th>
                                    <th>Sub-Category</th>
                                    <th>Sub-Category Order</th>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1;
                                ?>

                                @foreach($faqs['faq'] as $faq)
                                    <tr>
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $faq["category"] }}</td>
                                        <td class="text-center">{{ $faq["category_ordering"] }}</td>
                                        <td class="text-center">{{ $faq["subcategory"] }}</td>
                                        <td class="text-center">{{ $faq["subcategory_ordering"] }}</td>
                                        <td class="text-center">
                                            <p style="white-space: pre-line">
                                                {{ $faq["question"] }}
                                            </p>
                                        </td>
                                        <td class="text-left">
                                            <p style="white-space: pre-line">
                                                {{ $faq["answer"] }}
                                            </p>
                                        </td>
                                        <td class="text-left">
                                            <a href="{{ URL::to('/admin/'.$user_id.'/app-management/faq/edit/'.$faq["id"]) }}" type="btn btn-primary" role="button">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection