@extends('admin/admin')
@section('content')

<!-- Main content -->
<div class="row">


  <div class="col-md-12">
    <form method="POST" action="{{ URL::to('admin/'.$user_id.'/app-management/faq/store') }}" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="card">
        <div class="card-body">
          
          @if(!empty($errors->all()))
          <div class="alert alert-danger">
            {{ Html::ul($errors->all())}}
          </div>
          @endif


          <div class="">
            <h3>Add {{$title}}</h3>
          </div>
          <br>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group has-feedback">
                {{ Form::label('category', 'Category') }}
                <div class="form-group"> 
                    <input type="text" class="form-control" placeholder="Category" name="category" value="">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group has-feedback">
                {{ Form::label('subcategory', 'Sub Category') }}
                <div class="form-group"> 
                    <input type="text" class="form-control" placeholder="Sub Category" name="subcategory" value="">
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group has-feedback">
                {{ Form::label('category_ordering', 'Category Order') }}
                <div class="form-group"> 
                    <input type="text" class="form-control" placeholder="Category Order" name="category_ordering" value="">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group has-feedback">
                {{ Form::label('subcategory_ordering', 'Sub Category') }}
                <div class="form-group"> 
                    <input type="text" class="form-control" placeholder="Sub Category Order" name="subcategory_ordering" value="">
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group has-feedback">
                {{ Form::label('question', 'Question') }}
                <textarea style="white-space: pre-line" class="col-md-12" name="question" id="" rows="3"  placeholder="Question"></textarea>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group has-feedback">
                {{ Form::label('answer', 'Answer') }}
                <textarea style="white-space: pre-line" class="col-md-12 text-left" name="answer" id="" rows="3"  placeholder="Answer"></textarea>
              </div>
            </div>
          </div>

        </div><!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary pull-right">Add {{$title}}</button>
        </div>
      </div>
    </form>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- /.content -->

@endsection