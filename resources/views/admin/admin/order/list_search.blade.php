@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                    
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/tra$orders/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/orders/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                </div>
                            </form>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($expedition_name == null)
                                    Select Expedition
                                @else
                                    {{$expedition_name}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=grab_express&payment_method='.$payment_method) }}">Grab Express</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=gojek&payment_method='.$payment_method) }}">Go-Send</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=anteraja&payment_method='.$payment_method) }}">Anter Aja</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=paxel&payment_method='.$payment_method) }}">Paxel</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=sicepat&payment_method='.$payment_method) }}">Sicepat</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=jne&payment_method='.$payment_method) }}">JNE</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=jnt&payment_method='.$payment_method) }}">J&T</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=pos&payment_method='.$payment_method) }}">POS Indonesia</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=deliveree&payment_method='.$payment_method) }}">Deliveree</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=ninja&payment_method='.$payment_method) }}">Ninja Xpress</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=tiki&payment_method='.$payment_method) }}">Tiki</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=wahana&payment_method='.$payment_method) }}">Wahana</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=lion&payment_method='.$payment_method) }}">Lion Parcel</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=mrspeedy&payment_method='.$payment_method) }}">Mr Speedy</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=rpx&payment_method='.$payment_method) }}">RPX</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name=jet&payment_method='.$payment_method) }}">JET</a>
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($payment_method == null)
                                    Select Payment
                                @else
                                    {{$payment_method}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name='.$expedition_name.'&payment_method=OVO') }}">OVO</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name='.$expedition_name.'&payment_method=DANA') }}">DANA</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name='.$expedition_name.'&payment_method=LINKAJA') }}">LINKAJA</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name='.$expedition_name.'&payment_method=GOPAY') }}">GOPAY</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name='.$expedition_name.'&payment_method=SHOPEEPAY') }}">SHOPEEPAY</a>
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if($voucher_title == null)
                                        Select Voucher
                                    @else
                                        {{$voucher_title}}
                                    @endif
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach ($voucher_lists['voucher'] as $voucher)
                                    <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword.'&expedition_name='.$expedition_name.'&payment_method='.$payment_method.'&voucher_title='.$voucher['voucher_title']) }}">{{$voucher['voucher_title']}}</a>
                                    @endforeach
                                </div>
                            </div>
                    </div>
                    <br>
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    @if($access_level['role_id']==2)
                                        <th>Tanggal Order</th>
                                        <th>Nama Toko</th>
                                        <th>Expedition</th>
                                        <th>Status</th>
                                        <th>Shipping Price</th>
                                        <th>KP</th>
                                        <th>Ewallet</th>
                                        <th>Voucher</th>
                                        <th>Voucher Title</th>
                                        <th>Payment Method</th>
                                        <th>Expedition External Id</th>
                                        <th>Layanan</th>
                                        <th>Nomor Resi</th>
                                        <th>Berat</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Kode Invoice</th>
                                        <th>Reference Id</th>
                                        <th>Order Id</th>
                                        <th>Payment Id</th>
                                        <th>Shop Id</th>
                                        <th>Tanggal Invoice</th>
                                        <th>Description</th>
                                    @elseif($access_level['role_id']==1)
                                        <th>Tanggal Order</th>
                                        <th>Nama Toko</th>
                                        <th>Kode Invoice</th>
                                        <th>Expedition</th>
                                        <th>Layanan</th>
                                        <th>Ongkos Kirim</th>
                                        <th>Status</th>
                                        
                                    @endif
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>

                            <?php 
                                $GLOBAL['order_id'] = 0;
                                $no = 1 + (($orders['current_page']-1)*$orders['limit']);
                                $last_id = 0;
                                $i = 0;
                                // echo $orders
                            ?>

                                @foreach($orders['results'] as $payment)
                                        <tr>
                                                <td class="text-center">{{ $no++ }}</td>
                                                
                                                @if($access_level['role_id']==2)
                                                    @if(isset($payment['order3']))
                                                        <td class="text-center">{{ str_replace("T"," ",substr($payment['order3']['created_at'], 0, 19))}}</td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                    @endif
                                                    <td class="text-center">{{ $payment['trx']['shop']['name']}}</td>
                                                    

                                                    @if(isset($payment['order3']))
                                                        <td class="text-center">{{ $payment['order3']['expedition']['name']}}</td>
                                                        <td class="text-center
                                                        @if($payment['order3']['status'] == 'COMPLETED' or $payment['order3']['status'] == 'delivered' or $payment['order3']['status'] == 'completed')
                                                            {{'success'}}
                                                        @elseif($payment['order3']['status'] == 'FAILED' or $payment['order3']['status'] == 'cancelled' or $payment['order3']['status'] == 'cancelled_by_user' or $payment['order3']['status'] == 'failed' or $payment['order3']['status'] == 'courier_not_found')   
                                                            {{'error'}}
                                                        @else
                                                            {{'info'}}
                                                        @endif
                                                        ">{{ $payment['order3']['status']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['shipment_price']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['keytapoint_payment']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['ewallet_payment']}}</td>
                                                        <td class="text-center">{{ $payment['voucher_value']}}</td>
                                                        @if(isset($payment['order3']['voucher']))
                                                            <td class="text-center">{{ $payment['order3']['voucher']['voucher_title']}}</td>
                                                        @else
                                                            <td class="text-center">-</td>
                                                        @endif
                                                        @if($payment['order3']['ewallet_payment'] > 0 && $payment['order3']['keytapoint_payment'] > 0)
                                                            <td class="text-center">{{ "KEYTAPOINT + ".$payment['ewallet_type']}}</td>
                                                        @else
                                                            <td class="text-center">{{ $payment['ewallet_type']}}</td>
                                                        @endif
                                                        <td class="text-center">{{ $payment['order3']['expedition_external_number']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['expedition_service_type']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['resi_number']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['weight']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['sender_name']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['receipent_name']}}</td>
                                                        
                                                    @else
                                                        <td class="text-center">-</td>
                                                        <td class="text-center error">Order not created</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                    @endif

                                                    <td class="text-center">{{ $payment['trx']['number']}}</td>
                                                    
                                                    <td class="text-center">{{ $payment['external_id']}}</td>
                                                    @if(isset($payment['order3']))
                                                        <td class="text-center">{{ $payment['order3']['id']}}</td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                    @endif

                                                    <td class="text-center">{{ $payment['id']}}</td>
                                                    <td class="text-center">{{ $payment['trx']['shop_id']}}</td>
                                                    
                                                    <td class="text-center">{{ $payment['trx']['created_at']}}</td>
                                                    @if(count($payment['order3']['delivery_histories']) >= 1)
                                                        <td class="text-center">{{ $payment['order3']['delivery_histories'][count($payment['order3']['delivery_histories']) - 1]['description']}}</td>
                                                    @endif


                                                @elseif($access_level['role_id']==1)
                                                    @if(isset($payment['order3']))
                                                        <td class="text-center">{{ $payment['order3']['created_at']}}</td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                    @endif
                                                    
                                                    <td class="text-center">{{ $payment['trx']['shop']['name']}}</td>
                                                    <td class="text-center">{{ $payment['trx']['number']}}</td>

                                                    @if(isset($payment['order3']))
                                                        <td class="text-center">{{ $payment['order3']['expedition']['name']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['expedition_service_type']}}</td>
                                                        <td class="text-center">{{ $payment['order3']['total_price']}}</td>
                                                        <td class="text-center
                                                        @if($payment['order3']['status'] == 'COMPLETED' or $payment['order3']['status'] == 'delivered' or $payment['order3']['status'] == 'completed')
                                                            {{'success'}}
                                                        @elseif($payment['order3']['status'] == 'FAILED' or $payment['order3']['status'] == 'cancelled' or $payment['order3']['status'] == 'cancelled_by_user' or $payment['order3']['status'] == 'failed' or $payment['order3']['status'] == 'courier_not_found')   
                                                            {{'error'}}
                                                        @else
                                                            {{'info'}}
                                                        @endif
                                                        ">{{ $payment['order3']['status']}}</td>
                                                    @else
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center error">Order not created</td>
                                                    @endif
                                                @endif
                                        </tr>
                                        
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $orders['current_page'] + 2; 
                                    $start_page = $orders['current_page'] - 2;
                                ?>
                                
                                @if ($orders['current_page'] > 1)
                                    <?php $before_page = $orders['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/search/1?keyword='.$keyword) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/search/'.$before_page.'?keyword='.$keyword) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $orders['total_pages'])
                                        <li class="page-item @if($orders['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/search/'.$i.'?keyword='.$keyword) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($orders['current_page'] < $orders['total_pages'])
                                    <?php $next_page = $orders['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/search/'.$next_page.'?keyword='.$keyword) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/search/'.$orders['total_pages'].'?keyword='.$keyword) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection