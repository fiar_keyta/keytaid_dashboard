@extends('admin/admin')
@section('content')
<div class="modal fade" id="createDifferenceModal" tabindex="-1" role="dialog" aria-labelledby="createDifferenceModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createDifferenceModalLabel">Create new order difference</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ URL::to('admin/'.$user_id.'/orders/difference/create') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <!-- <div class="col-md-1"></div> -->
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                {{ Form::label('order_id', 'Order Id') }}
                                <div class="form-group"> 
                                    <input type="number" class="form-control" placeholder="order Id" name="order_id" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                {{ Form::label('difference_amount', 'Diff Amount') }}
                                <div class="form-group">
                                    <input type="number" class="form-control" placeholder="Difference Amount" name="difference_amount" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-md-1"></div> -->
                        <div class="col-md-12">
                            <div class="form-group has-feedback">
                                {{ Form::label('evidence_url', 'Evidence URL') }}
                                <div class="form-group"> 
                                    <input type="text" class="form-control" placeholder="https://" name="evidence_url" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="md-form active-cyan active-cyan-2 mb-3">
                        <button type="submit" class="btn btn-primary">Confirm Order Difference</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createDifferenceModal">
                        <i class="fa fa-plus" aria-hidden="true">   Order Difference</i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    @if($access_level['role_id']==1)
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Order Difference</span>
                                    <span class="info-box-number">{{ $orders['total'] }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">User Order Difference</span>
                                    <span class="info-box-number">{{ $orders['total_users'] }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($access_level['role_id']==2)
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Order Difference</span>
                                    <span class="info-box-number">{{ $orders['total'] }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">User Order Difference</span>
                                    <span class="info-box-number">{{ $orders['total_users'] }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Difference Amount</span>
                                    <span class="info-box-number">{{ "Rp. ".number_format($orders['order_value'], 0) }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <br> 
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    @if($access_level['role_id']==2)
                                        <th>Tanggal</th>
                                        <th>Shop Id</th>
                                        <th>Order Id</th>
                                        <th>Price</th>
                                        <th>Difference Amount</th>
                                        <th>Evidence URL</th>
                                        <th>Reference</th>
                                        <th>Status</th>
                                    @elseif($access_level['role_id']==1)
                                        <th>Tanggal</th>
                                        <th>Shop_id</th>
                                        <th>Order Id</th>
                                        <th>Price</th>
                                        <th>Difference Amount</th>
                                        <th>Evidence URL</th>
                                        <th>Reference Payment</th>
                                        <th>Status</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>

                            <?php 
                                $GLOBAL['order_id'] = 0;
                                $no = 1 + (($orders['current_page']-1)*$orders['limit']);
                                $last_id = 0;
                                $i = 0;
                                // echo $orders
                            ?>
                            @foreach($orders['results'] as $order)
                                <tr class="text-center">
                                    <td>{{ $no++ }}</td>
                                    <td>{{ str_replace("T"," ",substr($order['created_at'], 0, 19)) }}</td>
                                    <td>{{ $order['order']['trx']['shop_id'] }}</td>
                                    <td>{{ $order['order_id'] }}</td>
                                    <td>{{ $order['order']['total_price'] }}</td>
                                    <td>{{ $order['difference_amount'] }}</td>
                                    <td>{{ $order['evidence_url'] }}</td>
                                    <td>{{ $order['reference_id'] }}</td>
                                    <td>{{ $order['status'] }}</td>
                                </tr>
                            @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $orders['current_page'] + 2; 
                                    $start_page = $orders['current_page'] - 2;
                                ?>
                                @if($access_level['role_id']==2)
                                    @if ($orders['current_page'] > 1)
                                        <?php $before_page = $orders['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/1') }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/'.$before_page) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $orders['total_pages'])
                                            <li class="page-item @if($orders['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/'.$i) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($orders['current_page'] < $orders['total_pages'])
                                        <?php $next_page = $orders['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/'.$next_page) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/'.$orders['total_pages']) }}">Last</a></li>
                                    @endif
                                @elseif($access_level['role_id']==1)
                                    @if ($orders['current_page'] > 1)
                                        <?php $before_page = $orders['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/1') }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/'.$before_page) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $orders['total_pages'])
                                            <li class="page-item @if($orders['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/'.$i) }}">{{$i}}</a></li>
                                        @endif
                                    @endfor
                                    @if ($orders['current_page'] < $orders['total_pages'])
                                        <?php $next_page = $orders['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/'.$next_page) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders/difference/'.$orders['total_pages']) }}">Last</a></li>
                                    @endif
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection