@extends('admin/admin')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$title}}</h3>
                        
                        <div class="card-tools">
                            
                        </div>
                    </div>
                    <div class="card-body">
                        @if (Session::has('message'))
                            <div id="alert-msg" class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        <style>
                            a.disabled {
                                pointer-events: none;
                                color: #ccc;
                            }
                        </style>

                        <div class="row">
                            <form class="col-6" id="form_tanggal" method="GET"
                                action="{{ URL::to('/admin/'.$user_id.'/orders-premium/1?date_range='.$date_range) }}">
                                <div class="form-group">
                                    <label>Pilih Tanggal:</label>

                                    <div class="input-group">
                                        <button type="button" class="btn btn-default float-right" id="daterange-btn">
                                            <i class="fa fa-calendar"></i> {{$date_range}}
                                            <i class="fa fa-caret-down"></i>                                  
                                            <input type="hidden" name="date_range" id="tanggal" value="{{$date_range}}" />
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="row">
                            <div class="col-3 text-center">
                                <span></span>
                            </div>
                            <div class="col-1 text-center">
                                <a class="" href="{{URL::to('/admin/'.$user_id.'/orders-premium/export')}}" >
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </a>
                            </div>
                            <div class="col-5">
                                <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/orders-premium/search/1') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                    </div>
                                </form>
                            </div>
                        </div> 

                        <br> 
                        <div class="scrolling-wrapper">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th>No</th>
                                        <th>Tanggal Order</th>
                                        <th>Nama Toko</th>
                                        <th>Feature</th>
                                        <th>Expired</th>
                                        <th>Status</th>
                                        <th>Amount</th>
                                        <th>KP</th>
                                        <th>Ewallet</th>
                                        <th>Payment Method</th>
                                        <th>Reference Id</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                        $GLOBAL['order_id'] = 0;
                                        $no = 1 + (($orders['current_page']-1)*$orders['limit']);
                                        $last_id = 0;
                                        $i = 0;
                                        // echo $orders
                                    ?>

                                    @foreach($orders['results'] as $order)
                                            <tr>
                                                <td class="text-center">{{ $no++ }}</td>
                                                <td class="text-center">{{ str_replace("T"," ",substr($order['created_at'], 0, 19))}}</td>
                                                <td class="text-center">{{ $order['shops']['name']}}</td>
                                                <td class="text-center">{{ $order['details']['premium_feature']}}</td>
                                                <td class="text-center">{{ $order['details']['expired_date']}}</td>
                                                <td class="text-center">{{ $order['status']}}</td>
                                                <td class="text-center">{{ $order['amount']}}</td>
                                                <td class="text-center">{{ $order['keytapoint_payment']}}</td>
                                                <td class="text-center">{{ $order['ewallet_payment']}}</td>
                                                @if($order['ewallet_payment'] > 0 && $order['keytapoint_payment'] > 0)
                                                    <td class="text-center">{{ "KEYTAPOINT + ".$order['premium_payments']['ewallet_type']}}</td>
                                                @elseif($order['premium_payments']['external_id'] == null)
                                                    <td class="text-center">Trial</td>
                                                @else
                                                    <td class="text-center">{{ $order['premium_payments']['ewallet_type']}}</td>
                                                @endif
                                                <td class="text-center">{{ $order['premium_payments']['external_id']}}</td>  
                                            </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-2 d-flex justify-content-center">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <?php 
                                        $show_page = $orders['current_page'] + 2; 
                                        $start_page = $orders['current_page'] - 2;
                                    ?>
                                    @if ($orders['current_page'] > 1)
                                        <?php $before_page = $orders['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/1') }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/'.$before_page) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $orders['total_pages'])
                                            <li class="page-item 
                                                @if($orders['current_page'] == $i) active @endif">
                                                <a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/'.$i) }}">{{$i}}</a>
                                            </li>
                                        @endif
                                    @endfor
                                    @if ($orders['current_page'] < $orders['total_pages'])
                                        <?php $next_page = $orders['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/'.$next_page) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/orders-premium/'.$orders['total_pages']) }}">Last</a></li>
                                    @endif
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection