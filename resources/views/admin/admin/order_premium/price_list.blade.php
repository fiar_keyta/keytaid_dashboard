@extends('admin/admin')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$title}}</h3>
                        
                        <div class="card-tools">
                            <div class="card-tools">
                                <a class="btn btn-primary" href="{{ URL::to('/admin/'.$user_id.'/pricing-premium/add') }}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            </div>
                        </div> 
                    </div>
                    <div class="card-body">
                        @if (Session::has('message'))
                            <div id="alert-msg" class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        
                        <style>
                            a.disabled {
                                pointer-events: none;
                                color: #ccc;
                            }
                        </style>

                        <br> 
                        <div class="scrolling-wrapper">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th>No</th>
                                        <th>Feature</th>
                                        <th>Duration</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <td>Trial</td>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                        $no = 1;
                                        // echo $pricings
                                    ?>

                                    @foreach($pricings['results'] as $price)
                                            <tr>
                                                <td class="text-center">{{ $no++ }}</td>
                                                <td class="text-center">{{ $price['features']['name'] }}</td>
                                                <td class="text-center">{{ $price['duration']." ".$price['duration_type'] }}</td>
                                                @if($price['quantity'] == null)
                                                    <td class="text-center">-</td>
                                                @else
                                                    <td class="text-center">{{ $price['quantity']." ".$price['quantity_type'] }}</td>
                                                @endif
                                                <td class="text-center">{{ $price['price'] }}</td>
                                                <td class="text-center">{{ $price['is_trial'] }}</td>
                                                <td class="text-center">
                                                    <a href="{{URL::to('/admin/'.$user_id.'/pricing-premium/'.$price['id'].'/edit')}}">
                                                        <i class="fa fa-edit nav-icon fa-1x"></i>
                                                    </a>
                                                    <a href="{{URL::to('/admin/'.$user_id.'/pricing-premium/'.$price['id'].'/delete')}}">
                                                        <i class="fa fa-trash nav-icon fa-1x"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection