@extends('admin/admin')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$title}}</h3>
                        
                        <div class="card-tools">
                            
                        </div>
                    </div>
                    <div class="card-body"> 
                        @if (Session::has('message'))
                            <div id="alert-msg" class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        <style>
                            a.disabled {
                                pointer-events: none;
                                color: #ccc;
                            }
                        </style>

                        <div class="row">
                            <div class="col-3 text-center">
                                <span></span>
                            </div>
                            <div class="col-1 text-center">
                                <a class="disabled" href="{{URL::to('/admin/'.$user_id.'/orders/export')}}" >
                                    <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                                </a>
                            </div>
                            <div class="col-5">
                                <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/shops-premium/search/1') }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="md-form active-cyan active-cyan-2 mb-3">
                                        <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="">
                                    </div>
                                </form>
                            </div>
                        </div> 

                        <br> 
                        <div class="scrolling-wrapper">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th>No</th>
                                        <th>Status</th>
                                        <th>First Subs</th>
                                        <th>Expired Date</th>
                                        <th>Feature</th>
                                        <th>Shop Name</th>
                                        <th>Shop Phone</th>
                                        <th>Shop Id</th>
                                        <th>Action</th>
                                </thead>
                                <tbody>

                                    <?php 
                                        $GLOBAL['order_id'] = 0;
                                        $no = 1 + (($premiums['current_page']-1)*$premiums['limit']);
                                        $last_id = 0;
                                        $i = 0;
                                        // echo $premiums
                                    ?>

                                    @foreach($premiums['results'] as $premium)
                                            <?php 
                                                if(strtotime(date("Y-m-d")) <= strtotime($premium['expired_date'])){
                                                    $status = "Active";
                                                    $c_status = "success";
                                                }
                                                else {
                                                    $status = "Nonactive";
                                                    $c_status = "error";
                                                }
                                            ?>
                                            <tr>
                                                <td class="text-center">{{ $no++ }}</td>
                                                <td class="text-center {{$c_status}}">
                                                    {{$status}}
                                                </td>
                                                <td class="text-center">{{ str_replace("T"," ",substr($premium['created_at'], 0, 19))}}</td>
                                                <td class="text-center">{{ $premium['expired_date']}}</td>
                                                <td class="text-center">{{ $premium['premium_codes']['name']}}</td>
                                                <td class="text-center">{{ $premium['shop']['name']}}</td>
                                                <td class="text-center">{{ "+62 ".$premium['shop']['phone']}}</td>
                                                <td class="text-center">{{ $premium['shop']['id']}}</td>
                                            </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-2 d-flex justify-content-center">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <?php 
                                        $show_page = $premiums['current_page'] + 2; 
                                        $start_page = $premiums['current_page'] - 2;
                                    ?>
                                    @if ($premiums['current_page'] > 1)
                                        <?php $before_page = $premiums['current_page'] - 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/shops-premium/1') }}">First</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/shops-premium/'.$before_page) }}">Previous</a></li>
                                    @endif
                                    @for ($i = $start_page; $i <= $show_page; $i++)
                                        @if ($i > 0 and $i <= $premiums['total_pages'])
                                            <li class="page-item 
                                                @if($premiums['current_page'] == $i) active @endif">
                                                <a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/shops-premium/'.$i) }}">{{$i}}</a>
                                            </li>
                                        @endif
                                    @endfor
                                    @if ($premiums['current_page'] < $premiums['total_pages'])
                                        <?php $next_page = $premiums['current_page'] + 1 ?>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/shops-premium/'.$next_page) }}">Next</a></li>
                                        <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/shops-premium/'.$premiums['total_pages']) }}">Last</a></li>
                                    @endif
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection