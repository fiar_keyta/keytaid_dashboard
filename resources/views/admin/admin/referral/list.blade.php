@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif
                    
                    @if($access_level['role_id']==2)

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Referral Creation</span>
                                    <span class="info-box-number">{{ $users['total_data'] }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total User Send Referral</span>
                                    <span class="info-box-number">{{ $users['total_referrer'] }}</span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total User Pickup Courier</span>
                                    <span class="info-box-number">{{ $users['total_user_pk'] }}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endif
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Registered At</th>
                                    <th>Username</th>
                                    <th>Shop Id</th>
                                    <th>Shop Name</th>
                                    <th>Counter PK</th>
                                    <th>Nomor HP</th>
                                    <th>Activate Premium</th>
                                    <th>Expired Date</th>
                                    <th>Pricing</th>
                                    <th>Referrer Phone</th>
                                    <th>Referrer Name / Code</th>
                                    <th>Referrer Shop Id</th>
                                    <th>Referrer Shop Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($users['current_page']-1)*$users['limit']);
                                    // echo $users
                                ?>

                                @foreach($users['data'] as $user)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">{{ str_replace("T"," ",substr($user['created_at'], 0, 19)) }}</td>
                                    <td class="text-center">{{ $user['name'] }}</td>
                                    <td class="text-center">{{ $user['shop_id'] }}</td>
                                    <td class="text-center">{{ $user['shop']['name'] }}</td>
                                    <td class="text-center">{{ $user['shop']['total_pk'] }}</td>
                                    <td class="text-center">{{ $user['phone_with_code'] }}</td>
                                    @if( $user['shop']['premium'] == null  )
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                    @else
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">
                                        @foreach($user['shop']['premium'] as $pfeatures)
                                            @foreach($pfeatures['premium_feature'] as $pfeature)
                                                <li>{{ $pfeature}}</li>
                                            @endforeach
                                        @endforeach
                                    </td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">
                                        @foreach($user['shop']['premium'] as $pfeatures)
                                            @foreach($pfeatures['expired_date'] as $pfeature)
                                                <li>{{ $pfeature}}</li>
                                            @endforeach
                                        @endforeach
                                    </td>
                                    <td style="vertical-align : middle;text-align:center;" class="text-center">
                                        @foreach($user['shop']['premium'] as $pfeatures)
                                            @foreach($pfeatures['premium_price'] as $pfeature)
                                                <li>{{ $pfeature}}</li>
                                            @endforeach
                                        @endforeach
                                    </td>
                                    @endif
                                    <td class="text-center">{{ $user['shop']['referrer'][0]['phone_with_code'] }}</td>
                                    <td class="text-center">{{ $user['shop']['referrer'][0]['name'] }}</td>
                                    <td class="text-center">{{ $user['shop']['referrer'][0]['shop_id'] }}</td>
                                    @if(isset($user['shop']['referrer'][0]['shop']))
                                        <td class="text-center">{{ $user['shop']['referrer'][0]['shop']['name'] }}</td>
                                    @else
                                        <td class="text-center">-</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $users['current_page'] + 2; 
                                    $start_page = $users['current_page'] - 2;
                                ?>
                                
                                @if ($users['current_page'] > 1)
                                    <?php $before_page = $users['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/referral/1') }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/referral/'.$before_page) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $users['total_pages'])
                                        <li class="page-item @if($users['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/referral/'.$i) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($users['current_page'] < $users['total_pages'])
                                    <?php $next_page = $users['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/referral/'.$next_page) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/referral/'.$users['total_pages']) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection