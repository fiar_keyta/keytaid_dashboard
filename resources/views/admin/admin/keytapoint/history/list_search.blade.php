@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Keyta Balance</span>
                                <span class="info-box-number">{{$points['total_balance']}}</span>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-3 text-center">
                        <span></span>
                    </div>
                    <br>

                    <div class="col-6">
                        <form method="GET"
                            action="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/search/1') }}"
                            enctype="multipart/form-data">
                            
                            <div class="md-form active-cyan active-cyan-2 mb-3">
                                <input class="form-control" type="text" placeholder="Search by shop id"
                                    aria-label="Search" name="keyword" value="{{$keyword}}">
                            </div>
                        </form>
                    </div>
                    </div>
                                        
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Status</th>
                                    <th>Date Created</th>
                                    <th>Shop Id</th>
                                    <th>Shop Name</th>
                                    <th>Transaction Number</th>
                                    <th>Transaction Type</th>
                                    <th>Reference</th>
                                    <th>Amount</th>
                                    <th>Balance</th>    
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($points['current_page']-1)*$points['limit']);
                                    // echo $points
                                ?>

                                @foreach($points['results'] as $point)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">Completed</td>
                                    <td class="text-center">{{ $point['created_at'] }}</td>
                                    <td class="text-center">{{ $point['shop']['id'] }}</td>
                                    <td class="text-center">{{ $point['shop']['name'] }}</td>
                                    <td class="text-center">{{ $point['number'] }}</td>
                                    <td class="text-center">{{ $point['payment_type'] }}</td>
                                    <td class="text-center">{{ $point['id'] }}</td>
                                    <td class="text-center">{{ $point['amount'] }}</td>
                                    <td class="text-center">{{ $point['current_balance'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $points['current_page'] + 2; 
                                    $start_page = $points['current_page'] - 2;
                                ?>
                                
                                @if ($points['current_page'] > 1)
                                    <?php $before_page = $points['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/search/1?keyword='.$keyword) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/search/'.$before_page.'?keyword='.$keyword) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $points['total_pages'])
                                        <li class="page-item @if($points['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/search/'.$i.'?keyword='.$keyword) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($points['current_page'] < $points['total_pages'])
                                    <?php $next_page = $points['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/search/'.$next_page.'?keyword='.$keyword) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytapoint/history/search/'.$points['total_pages'].'?keyword='.$keyword) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection