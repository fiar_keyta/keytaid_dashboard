@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="text-center">
                                <th>No</th>
                                <th>Logo</th>
                                <th>Name</th>
                                <th>Partner</th>
                                <th>Current Waybill</th>
                                <th>Max Waybill</th>
                                <th>Ordering</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1 + (($expeditions['current_page']-1)*$expeditions['limit']);
                                // echo $expeditions
                            ?>

                            @foreach($expeditions['results'] as $expedition)
                            <tr>
                            
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">
                                        <img src="{{ $expedition['logo_url'] }}" alt="" width="100px">    
                                    </td>
                                    <td class="text-center">{{ $expedition['name'] }}</td>
                                    <td class="text-center">{{ $expedition['partner'] }}</td>
                                    <td class="text-center">{{ $expedition['current_waybill'] }}</td>
                                    <td class="text-center">{{ $expedition['max_waybill'] }}</td>
                                    <td class="text-center">{{ $expedition['ordering'] }}</td>
                                    <td class="text-center">
                                        <a href="{{URL::to('/admin/'.$user_id.'/expeditions/list/'.$expedition['id'].'/edit')}}">
                                            <i class="fa fa-edit nav-icon fa-1x"></i>
                                        </a>
                                    </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection