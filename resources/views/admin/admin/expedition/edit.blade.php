@extends('admin/admin')
@section('content')

<!-- Main content -->
<div class="row">


  <div class="col-md-12">
    <form method="POST" action="{{ URL::to('admin/'.$user_id.'/expeditions/list/'.$expeditions['results']['id'].'/update') }}" enctype="multipart/form-data">
      @method('PUT')
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">{{$title}}</h3>
          
          <div class="card-tools">
              
          </div>
        </div>
        <div class="card-body">
          
          @if(!empty($errors->all()))
          <div class="alert alert-danger">
            {{ Html::ul($errors->all())}}
          </div>
          @endif


          <div class="row align-items-end">
            <div class="col-md-12">
              <div class="text-center">
                <img src="{{$expeditions['results']['logo_url']}}" alt="">
              </div>
              <div class="form-group has-feedback">
                  <div class="form-group">
                      {{ Form::label('logo_url', 'Upload Image') }}
                      {{ Form::file('logo_url', ['class'=>'form-control']) }}  
                  </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <!-- <div class="col-md-1"></div> -->
            <div class="col-md-4">
              <div class="form-group has-feedback">
                  <div class="form-group has-feedback">
                      {{ Form::label('name', 'Expedition Name') }}
                      <input type="text" class="form-control" placeholder="Expedition Name" name="name" value="{{$expeditions['results']['name']}}">
                  </div>
              </div>
            </div>
            <div class="col-md-4">
                <div class="form-group has-feedback">
                    {{ Form::label('partner', 'Partner') }}
                    <div class="form-group"> 
                        <select name="partner" class="form-control" value>
                            <option selected=@if($expeditions['results']['partner'] == "BITESHIP"){{"selected"}}@endif >BITESHIP</option>
                            <option selected=@if($expeditions['results']['partner'] == "SICEPAT"){{"selected"}}@endif>SICEPAT</option>
                            <option selected=@if($expeditions['results']['partner'] == "ANTERAJA"){{"selected"}}@endif>ANTERAJA</option>
                            <option selected=@if($expeditions['results']['partner'] == "GRAB"){{"selected"}}@endif>GRAB</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group has-feedback">
                    <div class="form-group has-feedback">
                        {{ Form::label('code', 'Expedition Code') }}
                        <input type="text" class="form-control" placeholder="Expedition Id" name="code" value="{{$expeditions['results']['code']}}">
                    </div>
                </div>
            </div>
          </div>

          <div class="row">
            <!-- <div class="col-md-1"></div> -->
            <div class="col-md-6">
              <div class="form-group has-feedback">
                {{ Form::label('is_enable', 'Enable') }}
                <div class="form-group align-bottom"> 
                    <select name="is_enable" class="form-control" value>
                        <option>True</option>
                        <option>False</option>
                    </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group has-feedback">
                  <div class="form-group has-feedback">
                      {{ Form::label('ordering', 'Ordering') }}
                      <input type="text" class="form-control" placeholder="Ordering" name="ordering" value="{{$expeditions['results']['ordering']}}">
                  </div>
              </div>
            </div>
          </div>

          @if($expeditions['results']['partner'] == "SICEPAT")
            <div class="row">
              <!-- <div class="col-md-1"></div> -->
              <div class="col-md-6">
                <div class="form-group has-feedback">
                    <div class="form-group has-feedback">
                        {{ Form::label('current_waybill', 'Current Waybill') }}
                        <input type="text" class="form-control" placeholder="Current Waybill" name="current_waybill" value="{{$expeditions['results']['current_waybill']}}">
                    </div>
                </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group has-feedback">
                      <div class="form-group has-feedback">
                          {{ Form::label('max_waybill', 'Max Waybill') }}
                          <input type="text" class="form-control" placeholder="Max Waybill" name="max_waybill" value="{{$expeditions['results']['max_waybill']}}">
                      </div>
                  </div>
              </div>
            </div>
          @endif

        </div><!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary pull-right">Update {{$title}}</button>
        </div>
      </div>
    </form>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- /.content -->

@endsection