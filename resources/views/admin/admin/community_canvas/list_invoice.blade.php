@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div> 
                    @endif

                    @if($access_level['role_id']==2)

                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-12">
                                <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fa fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Invoice</span>
                                    <span class="info-box-number">{{$users['total']}}</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endif
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama toko</th>
                                    <th>Kode invoice</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Status Pembayaran</th>
                                    <th>Status Pengiriman</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($users['current_page']-1)*$users['limit']);
                                    // echo $users
                                ?>

                                @foreach($users['results'] as $user)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td class="text-center">{{ str_replace("T"," ",substr($user['created_at'], 0, 19)) }}</td>
                                    <td class="text-center">{{ $user['shop']['name'] }}</td>
                                    <td class="text-center">{{ $user['number'] }}</td>
                                    <td class="text-center">{{ $user['customer']['customer_name'] }}</td>
                                    <td class="text-center">{{ $user['payment_status'] }}</td>
                                    <td class="text-center">{{ $user['shipment_status'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $users['current_page'] + 2; 
                                    $start_page = $users['current_page'] - 2;
                                ?>
                                
                                @if ($users['current_page'] > 1)
                                    <?php $before_page = $users['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/community/inv/1') }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/community/inv/'.$before_page) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $users['total_pages'])
                                        <li class="page-item @if($users['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/community/inv/'.$i) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($users['current_page'] < $users['total_pages'])
                                    <?php $next_page = $users['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/community/inv/'.$next_page) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/keytauser/community/inv/'.$users['total_pages']) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".check_all").on("click", function(){
            $(".shop_id").each(function(){
                $(this).attr("checked", true);
                if $(this).attr("checked", true){
                    $(this).attr("checked", false)
                }
                else{
                    $(this).attr("checked", true)
                }
            });
        });
    </script>
@endsection