@extends('admin/admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                    
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/tra$transactions/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/tra$transactions/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                </div>
                            </form>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($channel == null)
                                    Select Channel
                                @else
                                    {{$channel}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=instagram&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Instagram</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=whatsapp&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Whatsapp</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=whatsapp_bussiness&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Whatsapp Business</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=facebook_messenger&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Facebook Messenger</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=line&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Line</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=line_official&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Line Official</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=tiktok&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Tiktok</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=wechat&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Wechat</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=telegram&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Telegram</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=tokopedia&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Tokopedia</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=shopee&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Shopee</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel=bukalapak&expedition_name='.$expedition_name.'&payment_method='.$payment_method) }}">Bukalapak</a>
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($expedition_name == null)
                                    Select Expedition
                                @else
                                    {{$expedition_name}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=grab_express&payment_method='.$payment_method) }}">Grab Express</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=gojek&payment_method='.$payment_method) }}">Go-Send</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=anteraja&payment_method='.$payment_method) }}">Anter Aja</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=paxel&payment_method='.$payment_method) }}">Paxel</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=sicepat&payment_method='.$payment_method) }}">Sicepat</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=jne&payment_method='.$payment_method) }}">JNE</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=jnt&payment_method='.$payment_method) }}">J&T</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=pos&payment_method='.$payment_method) }}">POS Indonesia</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=deliveree&payment_method='.$payment_method) }}">Deliveree</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=ninja&payment_method='.$payment_method) }}">Ninja Xpress</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=tiki&payment_method='.$payment_method) }}">Tiki</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=wahana&payment_method='.$payment_method) }}">Wahana</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=lion&payment_method='.$payment_method) }}">Lion Parcel</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=mrspeedy&payment_method='.$payment_method) }}">Mr Speedy</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=rpx&payment_method='.$payment_method) }}">RPX</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name=jet&payment_method='.$payment_method) }}">JET</a>
                            </div>
                        </div>
                        <div class="col-2 dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($payment_method == null)
                                    Select Payment
                                @else
                                    {{$payment_method}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=BCA') }}">BCA</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=Mandiri') }}">Mandiri</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=BNI') }}">BNI</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=BRI') }}">BRI</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=BTPN') }}">BTPN</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=CIMB') }}">CIMB</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=Danamon') }}">Danamon</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=HSBC') }}">HSBC</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=Panin') }}">Panin</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=DBS') }}">DBS</a>
                                <a class="dropdown-item" href="{{ URL::to('/admin/'.$user_id.'/transactions/1?channel='.$channel.'&expedition_name='.$expedition_name.'&payment_method=Mega') }}">Mega</a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Tanggal Invoice</th>
                                    <th>Kode Invoice</th>
                                    <th>Nama Tokor</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Channel</th>
                                    <th>Channel Phone/ID</th>
                                    <th>Expedition</th>
                                    <th>Layanan</th>
                                    <th>Shipping Price</th>
                                    <th>Total Price</th>
                                    <th>Payment Method</th>
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($transactions['current_page']-1)*$transactions['limit']);
                                    // echo $transactions
                                ?>

                                @foreach($transactions['result'] as $transaction)
                                <tr>
                                
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $transaction['created_at'] }}</td>
                                        <td class="text-center">{{ $transaction['number'] }}</td>
                                        <td class="text-center">{{ $transaction['shop']['name'] }}</td>
                                        <td class="text-center">{{ $transaction['customer']['customer_name'] }}</td>
                                        <td class="text-center">{{ $transaction['customer']['channel'] }}</td>
                                        <td class="text-center">{{ $transaction['customer']['channel_account'] }}</td>
                                        <td class="text-center">{{ $transaction['expedition']['name'] }}</td>
                                        <td class="text-center">{{ $transaction['expedition']['expedition_service_type'] }}</td>
                                        <td class="text-center">{{ $transaction['shipping_price'] }}</td>
                                        <td class="text-center">{{ $transaction['total_price'] }}</td>
                                        <td class="text-center">{{ $transaction['bank']['name'] }}</td>
                                        
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $transactions['current_page'] + 2; 
                                    $start_page = $transactions['current_page'] - 2;
                                ?>
                                
                                @if ($transactions['current_page'] > 1)
                                    <?php $before_page = $transactions['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/$transactions/search/1?keyword='.$keyword) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/$transactions/search/'.$before_page.'?keyword='.$keyword) }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $transactions['total_pages'])
                                        <li class="page-item @if($transactions['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/$transactions/search/'.$i.'?keyword='.$keyword) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($transactions['current_page'] < $transactions['total_pages'])
                                    <?php $next_page = $transactions['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/$transactions/search/'.$next_page.'?keyword='.$keyword) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/$transactions/search/'.$transactions['total_pages'].'?keyword='.$keyword) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection