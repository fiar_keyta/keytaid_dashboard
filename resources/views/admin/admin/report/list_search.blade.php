@extends('admin/admin')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    
                    <div class="card-tools">
                        
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div id="alert-msg" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-3 text-center">
                            <span></span>
                        </div>
                        <div class="col-1 text-center">
                            <a href="{{URL::to('/admin/'.$user_id.'/reports/export')}}">
                                <i class="fa fa-file-excel-o nav-icon fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-5">
                            <form method="GET" action="{{ URL::to('/admin/'.$user_id.'/reports/search/1') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="md-form active-cyan active-cyan-2 mb-3">
                                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="keyword" value="{{$keyword}}">
                                </div>
                            </form>
                        </div>
                    </div> 

                    <br>
                    <div class="scrolling-wrapper">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Kode Invoice</th>
                                    <th>Subject</th>
                                    <th>Shop Id</th>
                                    <th>Nama Toko</th>
                                    <th>Nomor Telepon</th>
                                    <th>Reference Id</th>
                                    <th>Metode Pembayaran</th>
                                    <th>Jumlah Refund</th>
                                    <th>Nomor Akun</th>
                                    <th>Deskripsi</th>
                                    <th>Kategori</th>
                                    <th>Fitur</th>
                                    <th>Status</th>
                                    <th>Tanggal Laporan</th>
                                    <th>Action</th>
                                    <!-- <th>Action</th>  -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1 + (($reports['current_page']-1)*$reports['limit']);
                                    // echo $reports
                                ?>

                                @foreach($reports['result'] as $report)
                                <tr>
                                
                                        <td class="text-center">{{ $no++ }}</td>
                                        <td class="text-center">{{ $report['invoice_number'] }}</td>
                                        <td class="text-center">{{ $report['subject'] }}</td>
                                        @if(isset($report['trx']))
                                            <td class="text-center">{{ $report['trx']['shop']['id'] }}</td>
                                            <td class="text-center">{{ $report['trx']['shop']['name'] }}</td>
                                            <td class="text-center">{{ $report['trx']['shop']['phone'] }}</td>
                                            <td class="text-center">{{ $report['trx']['external_id'] }}</td>
                                        @else
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                        @endif
                                        
                                        <td class="text-center">{{ $report['payment_method'] }}</td>
                                        <td class="text-center">{{ $report['refund'] }}</td>
                                        <td class="text-center">{{ $report['account_number'] }}</td>
                                        <td class="text-center">{{ $report['description'] }}</td>
                                        <td class="text-center">{{ $report['kategori'] }}</td>
                                        <td class="text-center">{{ $report['fitur']}}</td>
                                        <td class="text-center">{{ $report['is_completed'] ? 'true' : 'false' }}</td>
                                        <td class="text-center">{{ $report['created_at']}}</td>
                                        <td class="text-center">
                                            <form action="{{URL::to('/admin/'.$user_id.'/reports/resolve/'.$report['id'])}}" method="post" enctype="multipart/form-data">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                @if($report['is_completed'] == TRUE)
                                                <button class="btn-error" disabled><i class="fa fa-check nav-icon fa-2x" type="submit" value="Submit"></i></button>
                                                @else
                                                    <button class="btn-success"><i class="fa fa-check nav-icon fa-2x" type="submit" value="Submit"></i></button>
                                                @endif
                                            </form> 
                                        </td>
                                        
                                        
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-2 d-flex justify-content-center">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <?php 
                                    $show_page = $reports['current_page'] + 2; 
                                    $start_page = $reports['current_page'] - 2;
                                ?>
                                
                                @if ($reports['current_page'] > 1)
                                    <?php $before_page = $reports['current_page'] - 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/report/search/1) }}">First</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/report/search/'.$before_page.') }}">Previous</a></li>
                                @endif
                                @for ($i = $start_page; $i <= $show_page; $i++)
                                    @if ($i > 0 and $i <= $reports['total_pages'])
                                        <li class="page-item @if($reports['current_page'] == $i) active @endif"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/report/search/'.$i) }}">{{$i}}</a></li>
                                    @endif
                                @endfor
                                @if ($reports['current_page'] < $reports['total_pages'])
                                    <?php $next_page = $reports['current_page'] + 1 ?>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/report/search/'.$next_page) }}">Next</a></li>
                                    <li class="page-item"><a class="page-link" href="{{ URL::to('/admin/'.$user_id.'/report/search/'.$reports['total_pages']) }}">Last</a></li>
                                @endif
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection