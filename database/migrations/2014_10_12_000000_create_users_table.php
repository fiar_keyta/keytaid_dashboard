<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('role_id')->nullable();
            $table->unsignedInteger('tenant_id')->nullable();
            $table->string('name');
            $table->string('username',100)->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('status')->nullable();
            $table->timestamps();
        });

        Schema::create('roles', function (Blueprint $kolom) {
            $kolom->increments('id');
            $kolom->string('access_level');
        });

        Schema::create('tenants', function (Blueprint $kolom) {
            $kolom->increments('id');
            $kolom->string('tenant_type')->nullable();
            $kolom->string('tenant_name')->nullable();
            $kolom->text('tenant_logo')->nullable();
            $kolom->string('full_name')->nullable();
            $kolom->text('image')->nullable();
            $kolom->string('place_of_birth')->nullable();
            $kolom->string('birthday')->nullable();
            $kolom->string('last_education')->nullable();
            $kolom->string('gender')->nullable();
            $kolom->string('home_address')->nullable();
            $kolom->string('handphone')->nullable();
            $kolom->string('email')->nullable();
            $kolom->string('bussiness_address')->nullable();
            $kolom->string('bussiness_permit')->nullable();
            $kolom->string('product_sertification')->nullable();
            $kolom->string('office_phone')->nullable();
            $kolom->string('bussiness_field')->nullable();
            $kolom->string('bussiness_start')->nullable();
            $kolom->string('funding')->nullable();
            $kolom->string('product')->nullable();
            $kolom->text('product_image')->nullable();
            $kolom->string('production_capacity')->nullable();
            $kolom->string('turnover')->nullable();
            $kolom->string('market_reach')->nullable();
            $kolom->string('worker')->nullable();
            $kolom->text('plan')->nullable();
            $kolom->timestamps();
        });

        Schema::create('uploads', function (Blueprint $kolom) {
            $kolom->increments('id');
            $table->unsignedInteger('tenant_id')->nullable();
            $kolom->string('filename')->nullable();
            $kolom->string('filepath')->nullable();
            $kolom->timestamps();
        });

        Schema::table('users', function (Blueprint $kolom)
        {
            $kolom->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');
            $kolom->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('uploads', function (Blueprint $kolom)
        {
            $kolom->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('roles');
    }
}
